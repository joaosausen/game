/**
 * @file Database operations.
 *
 * This file contains code referenced to database persistence.
 */

#include "db.hpp"

/** Start and create the initial database if it doesn't exist. */
int setup_db(sqlite3 * db) {
  char * error_msg = 0;
  std::vector<std::string> stmts = {
    {"CREATE TABLE IF NOT EXISTS user ("
     "uid INTEGER PRIMARY KEY AUTOINCREMENT, "
     "login CHAR(50) NOT NULL UNIQUE, "
     "pass CHAR(50) NOT NULL);" },
    {"CREATE TABLE IF NOT EXISTS entity ("
     "eid INTEGER PRIMARY KEY AUTOINCREMENT, "
     "type INT NOT NULL, "
     "name TEXT NOT NULL);"},
  };

  for (std::string stmt : stmts) {
    const int rc = sqlite3_exec(db, stmt.c_str(), nullptr, 0, &error_msg);
    if (rc != SQLITE_OK) {
      std::cout << KRED << __FILE__ << " " << __LINE__ << " [Error] SQL error: " << error_msg << RST << '\n';
      sqlite3_free(error_msg);
      std::cout << FRED("Failed to create initial database.") << '\n';
      return 1;
    }
  }

  return 0;
}

/**
 * Create a user given a login and password. */
int db_create_user(sqlite3 * db, std::string login, std::string pass) {
  char * error_msg = 0;
  std::stringstream sql;
  sql << "INSERT INTO user (login, pass) VALUES ('" << login << "', '" << pass << "');";
  const int rc = sqlite3_exec(db, sql.str().c_str(), nullptr, 0, &error_msg);
  if (rc != SQLITE_OK) {
    std::cout << KRED << __FILE__ << " " << __LINE__ << " [Error] SQL error: " << error_msg << RST << '\n';
    sqlite3_free(error_msg);
    return 1;
  }
  return 0;
}

/**
 * Return uid given a login. */
int db_user_get_uid_from_login(sqlite3 * db, std::string login) {
  std::stringstream sql;
  sql << "SELECT uid FROM user WHERE login = '" << login << "';";
  sqlite3_stmt *stmt;
  int rc = sqlite3_prepare_v2(db, sql.str().c_str(), -1, &stmt, NULL);
  if (rc != SQLITE_OK) {
    std::cout << KRED << __FILE__ << " " << __LINE__ << " [Error] SQL error: " << sqlite3_errmsg(db) << RST << '\n';
    sqlite3_finalize(stmt);
    return -1;
  }

  // There should be only one item at most.
  while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
    auto uid = sqlite3_column_int(stmt, 0);
    sqlite3_finalize(stmt);
    return uid;
  }
  sqlite3_finalize(stmt);
  return -1;
}

/**
 * Try to login and return the user uid. */
int db_try_login(sqlite3 * db, std::string login, std::string pass) {
  // Do not check for password, if login doesn't exist, we will create it.
  std::stringstream sql;
  sql << "SELECT uid, login, pass FROM user WHERE login = '" << login << "';";
  sqlite3_stmt *stmt;

  int rc = sqlite3_prepare_v2(db, sql.str().c_str(), -1, &stmt, NULL);
  if (rc != SQLITE_OK) {
    std::cout << KRED << __FILE__ << " " << __LINE__ << " [Error] SQL error: " << sqlite3_errmsg(db) << RST << '\n';
    return -2;
  }

  // There should be only one item at most.
  bool found = false;
  while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
    std::stringstream row_pass;
    row_pass << sqlite3_column_text(stmt, 2);
    if (pass.compare(row_pass.str()) == 0) {
      // Login found and password matches, return uid.
      auto uid = sqlite3_column_int(stmt, 0);
      sqlite3_finalize(stmt);
      return uid;
    }
    found = true;
    break;
  }

  sqlite3_finalize(stmt);

  if (found) {
    // User found but password didnt match, return error.
    return -1;
  }

  // Create user, return uid with that user.
  if (db_create_user(db, login, pass) == 1) {
    // Error creating user.
    return -2;
  }

  const int uid = db_user_get_uid_from_login(db, login);
  return uid >= 0 ? uid : -2;
}
