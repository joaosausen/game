#include <sstream>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <iostream>
#include <cmath>

#include "callbacks.hpp"
#include "db.hpp"

/** Extract a string from buffer. */
std::string extract_string_from_buffer(const unsigned char buffer[1024], const int start, const int length) {
  std::string str;
  copy(buffer + start, buffer + start + length, std::back_inserter(str));
  return str;
}

/** Broadcast packet to all entities on stage. */
void stage_broadcast(entt::registry &registry, entt::entity stage, Packet packet) {
  if (stage == entt::null) {
    return;
  }
  registry.view<entity_component, client_component>().each([stage, &packet](auto entity, auto &ent, auto &cli) {
    if (ent.stage == stage) {
      packet_send(cli.socket, packet);
    }
  });
}

/** Wrapper for send */
void packet_send(int socket, Packet packet) {
  send(socket, packet.buffer.c_str(), packet.buffer.length(), 0);
}

/**
 * Requests.
 *
 * Data sent to the socket.
 * @see packet_db.js
 */

/**
 * This same function exists on the client.
 */
int _pack_float(float number) {
  return static_cast<int>(number * 100);
}

/**
 * This same function exists on the client.
 */
float _unpack_float(float number) {
  return number / 100;
}

Packet packet_create_entity(entt::entity id, int id_type, int alignment, int x, int y) {
  Packet packet;
  packet << static_cast<int>(1) << static_cast<int>(id) << id_type << alignment << x << y;
  return packet;
}

Packet packet_entity_move(entt::entity id, float x, float y, float direction_x, float direction_y, float velocity) {
  Packet packet;
  packet << static_cast<int>(2) << static_cast<int>(id) << _pack_float(x) << _pack_float(y) << _pack_float(direction_x) << _pack_float(direction_y) << _pack_float(velocity);
  return packet;
}

Packet packet_login_successful() {
  Packet packet;
  packet << static_cast<int>(7);
  return packet;
}

Packet packet_delete_entity(entt::entity id) {
  Packet packet;
  packet << static_cast<int>(3) << static_cast<int>(id);
  return packet;
}

Packet packet_load_map(int map_id) {
  Packet packet;
  packet << static_cast<int>(6) << map_id;
  return packet;
}

Packet packet_popup_message(int type, std::string message) {
  Packet packet;
  packet << static_cast<int>(4) << type << message;
  return packet;
}

Packet packet_stage_list(entt::registry & registry) {
  Packet packet;
  // @todo calculate players online.
  registry.view<stage_component>().each([&packet](auto e, auto &sta) {
    packet << static_cast<int>(5) << static_cast<int>(e) << static_cast<int>(sta.online);
  });
  return packet;
}

/**
 * Callbacks.
 *
 * Data received from the socket.
 */

/** Disconnect and delete data from registry. */
void disconnect_socket(socket_buffer sb, entt::registry &registry) {
  const auto socket = sb.socket;
  auto &ce_map = registry.ctx<socket_entity_map>();
  if (ce_map.find(socket) != ce_map.end()) {
    // Load entity from map.
    auto entity = ce_map[socket];
    auto &ent = registry.get<entity_component>(entity);
    // Craft packet and send to stage to delete entity.
    auto packet = packet_delete_entity(entity);
    stage_broadcast(registry, ent.stage, packet);
    // Delete entity from server.
    ce_map.erase(socket);
    registry.destroy(entity);

    auto sta = registry.try_get<stage_component>(ent.stage);
    if (sta != nullptr) {
      if (sta->online > 0) {
        sta->online--;
      }
    }
  }
  close(socket);
}

/** Create entity and assign to stage, if it already exists, just add to
 * a different stage. */
void setup_user(socket_buffer sb, entt::registry &registry, const int stage, entt::entity entity) {
  const auto socket = sb.socket;
  if (entity != entt::null) {
    // If entity exists, broadcast on client stage to delete that entity, then
    // change entity stage.
    auto delete_entity_packet = packet_delete_entity(entity);
    auto &ent = registry.get<entity_component>(entity);
    stage_broadcast(registry, ent.stage, delete_entity_packet);
    // Decrease online count on old stage.
    auto sta_old = registry.try_get<stage_component>(ent.stage);
    if (sta_old != nullptr) {
      sta_old->online--;
    }
    // Change stage.
    ent.stage = static_cast<entt::entity>(stage);
    // Increase online count on new stage.
    auto sta = registry.try_get<stage_component>(ent.stage);
    if (sta != nullptr) {
      sta->online++;
    }
  }
  else {
    // Create entity on registry and assign to map.
    entity = registry.create();
    std::cout << "Creating entity " << (int) entity << '\n';
    registry.assign<client_component>(entity, socket);
    // @todo properly load name.
    registry.assign<entity_component>(entity, 1, "Frida", 0.f, 0.f, static_cast<entt::entity>(stage));
    registry.assign<movement_component>(entity, 0.f, 0.f, 0.f, 100.f, 0.f);
    
    auto &ce_map = registry.ctx<socket_entity_map>();
    ce_map.insert({socket, entity});
    // Increase online count.
    auto sta = registry.try_get<stage_component>(static_cast<entt::entity>(stage));
    if (sta != nullptr) {
      sta->online++;
    }
  }

  // @todo properly create maps, for now its just to force the player to delete
  // all entities.
  auto load_map_packet = packet_load_map(1);
  packet_send(socket, load_map_packet);

  // Send a packet to the user to create/update his own entity.
  auto myself = packet_create_entity(entity, 1, 0, 0, 0);
  packet_send(socket, myself);

  // Packet to send to all other socket to create this entity.
  auto packet = packet_create_entity(entity, 1, 1, 0, 0);
  const auto [socket_ent, socket_cli] = registry.get<entity_component, client_component>(entity);
  registry.view<entity_component, client_component>().each([entity, socket_ent, socket_cli, &packet](auto e, auto &ent, auto &cli) {
    if (ent.stage == socket_ent.stage) {
      if (entity != e) {
        // Send to everyone a packet to create the socket's entity.
        packet_send(cli.socket, packet);
        // Send to the socket a packet to create this iteration entity.
        auto iteration_packet = packet_create_entity(e, ent.id_type, 1, ent.x, ent.y);
        packet_send(socket_cli.socket, iteration_packet);
      }
    }
  });

  // Send the ball entity to the user that just connected.
  registry.view<entity_component, ball_component>().each([socket_ent, socket_cli](auto e, auto &ent, auto &bal) {
    if (ent.stage == socket_ent.stage) {
      // Send to the socket a packet to create the ball.
      auto ball_packet = packet_create_entity(e, ent.id_type, 2, ent.x, ent.y);
      packet_send(socket_cli.socket, ball_packet);
    }
  });
}

void request_login(socket_buffer sb, entt::registry &registry, sqlite3 * db) {
  const auto buffer = sb.buffer;
  const auto socket = sb.socket;
  const int user_size = extract_from_buffer(sb, 4);
  const std::string user = extract_string_from_buffer(buffer, 8, user_size);
  const int pass_size = extract_from_buffer(sb, 8 + user_size);
  const std::string pass = extract_string_from_buffer(buffer, 12 + user_size, pass_size);
  std::cout << "user: " << user << " password: " << pass << '\n';

  // Validate.
  if (user.empty() || pass.empty()) {
    auto packet = packet_popup_message(0, "Login and password are required.");
    packet_send(socket, packet);
    return;
  }

  const int db_result = db_try_login(db, user, pass);

  if (db_result == -1) {
    auto packet = packet_popup_message(0, "Invalid login/password.");
    packet_send(socket, packet);
    return;
  }
  else if (db_result == -2) {
    auto packet = packet_popup_message(0, "There was an error retrieving your user.");
    packet_send(socket, packet);
    return;
  }

  auto &ce_map = registry.ctx<socket_entity_map>();
  if (ce_map.find(socket) == ce_map.end()) {
    // User successfully connected.
    auto packet = packet_login_successful();
    packet_send(socket, packet);
  }
  else {
    // User is trying to login but its still logged in, send the logged in user
    // a disconnect.
    disconnect_socket(sb, registry);
  }
}

/** User is requesting the stage list. */
void request_stage_list(socket_buffer sb, entt::registry &registry, entt::entity entity) {
  // Entity will be null if user is requesting the stage list just after login.
  if (entity != entt::null) {
    // Update stage count and remove user from stage.
    auto &ent = registry.get<entity_component>(entity);
    auto sta = registry.try_get<stage_component>(ent.stage);
    if (sta != nullptr) {
      if (sta->online > 0) {
        sta->online--;
      }
    }
    ent.stage = entt::null;
    // Request other clients to remove entities.
    auto delete_packet = packet_delete_entity(entity);
    stage_broadcast(registry, ent.stage, delete_packet);
  }

  const auto socket = sb.socket;
  auto packet = packet_stage_list(registry);
  packet_send(socket, packet);
}

/** User is requesting to join stage. */ // @todo extract entt::entity instead of int.
void request_select_stage(socket_buffer sb, entt::registry &registry, entt::entity entity) {
  const auto socket = sb.socket;
  int stage_id = extract_from_buffer(sb, 4);
  auto stage = registry.try_get<stage_component>(static_cast<entt::entity>(stage_id));
  if (stage == nullptr) {
    // socket is trying to join a stage that doesn't exists.
    auto packet = packet_popup_message(0, "Invalid stage, relog and try again.");
    packet_send(socket, packet);
    return;
  }
  // Setup user if it doesn't have an entity yet.
  setup_user(sb, registry, stage_id, entity);
}

/** Move an entity on direction x, y */
void _set_entity_direction(entt::registry &registry, entt::entity entity, float x, float y) {
  // Evaluate.
  // Ideally the values send on x and y will be equal to the sides of a triangle
  // rectangle where hypotenuse is 1, hypotenuse will be the full movement the
  // entity will do, reduced to 1 because it will be multiplied by velocity.
  // Since these values are calculated with javascript, its very likely that the
  // hypotenuse will not be 1.
  if (x != 0 || y != 0) {
    float hypotenuse = sqrt(x*x + y*y);
    auto proportion_to_reduce = 1 / hypotenuse;
    x = x * proportion_to_reduce;
    y = y * proportion_to_reduce;
  }

  auto &mov = registry.get<movement_component>(entity);
  mov.direction_x = x;
  mov.direction_y = y;
  mov.velocity = mov.max_velocity;
}

/** User is requesting to move own entity. */
void request_move(socket_buffer sb, entt::registry &registry, entt::entity entity) {
  // These are triangle side values, they should evaluate to a hypothenuse of 1.
  float x = _unpack_float(extract_from_buffer(sb, 4));
  float y = _unpack_float(extract_from_buffer(sb, 8));
  std::cout << "MOVE: " << x << ' ' << y << '\n';
  _set_entity_direction(registry, entity, x, y);
}

/** User is requesting to kick the ball. */
void request_kick_ball(socket_buffer sb, entt::registry &registry, entt::entity entity) {
  if (entity == entt::null) {
    return;
  }

  // Position the player clicked.
  int click_x = extract_from_buffer(sb, 4);
  int click_y = extract_from_buffer(sb, 8);
  auto &socket_ent = registry.get<entity_component>(entity);

  // Move the ball.
  registry.view<entity_component, movement_component, ball_component>().each([socket_ent, click_x, click_y, &registry](auto e, auto &ent, auto &mov, auto &bal) {
    if (ent.stage == socket_ent.stage) {
      // Calculate the ball direction and set a velocity.
      float x = click_x - ent.x;
      float y = click_y - ent.y;
      float hypotenuse = sqrt(x*x + y*y);
      mov.direction_x = x / hypotenuse;
      mov.direction_y = y / hypotenuse;
      mov.velocity = mov.max_velocity;
    }
  });

}
