#include "server.hpp"

#define MAXEVENTS 64
#define PORT 5000

// @todo fix this atrocity using templates.
Packet& Packet::operator<<(const std::string data) {
  // Push string size first, then append the string.
  int size = data.length();
  (*this) << size;
  buffer.append(data);
  return *this;
}

Packet& Packet::operator<<(const entt::entity data) {
  // Breaks data into array of chars and append to buffer.
  int copy = static_cast<int>(data);
  unsigned char * bytes = reinterpret_cast<unsigned char *>(&copy);
  // Big-endian.
  for (int i = sizeof(copy) - 1; i >= 0; i--) {
    buffer.push_back(bytes[i]);
  }
  return *this;
}

Packet& Packet::operator<<(const int data) {
  // Breaks data into array of chars and append to buffer.
  int copy = data;
  unsigned char * bytes = reinterpret_cast<unsigned char *>(&copy);
  // Big-endian.
  for (int i = sizeof(copy) - 1; i >= 0; i--) {
    buffer.push_back(bytes[i]);
  }
  return *this;
}

Packet& Packet::operator<<(const unsigned short data) {
  // Breaks data into array of chars and append to buffer.
  unsigned short copy = data;
  unsigned char * bytes = reinterpret_cast<unsigned char *>(&copy);
  // Big-endian.
  for (int i = sizeof(copy) - 1; i >= 0; i--) {
    buffer.push_back(bytes[i]);
  }
  return *this;
}

Packet& Packet::operator<<(const unsigned char data) {
  // Breaks data into array of chars and append to buffer.
  unsigned char copy = data;
  unsigned char * bytes = reinterpret_cast<unsigned char *>(&copy);
  // Big-endian.
  for (int i = sizeof(copy) - 1; i >= 0; i--) {
    buffer.push_back(bytes[i]);
  }
  return *this;
}

/** Extract 4 bytes from buffer and return as integer. */
int extract_from_buffer(socket_buffer sb, const int start) {
  auto buffer = sb.buffer;
  int integer = 0;
  for (int i = start; i < start + 4; i++) {
    integer <<= 8;
    integer |= buffer[i];
  }
  return integer;
}

/**
 * Set socket non blocking. */
int set_nonblocking(int fd) {
  int flags = fcntl(fd, F_GETFL, 0);
  if (flags == -1) {
    std::cout << FRED("There was an error retrieving flags to set a socket non blocking.") << '\n';
    return 1;
  }
  if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
    std::cout << FRED("There was an error setting flags to make a socket non blocking.") << '\n';
    return 1;
  }
  return 0;
}

/**
 * Properly start the server. */
int start_server(moodycamel::ReaderWriterQueue<socket_buffer> & queue) {
  // create the server socket
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock == -1) {
    std::cout << FRED("[Error] Can't create socket.") << '\n';
    return 1;
  }

  int enable = 1;
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) == -1) {
    std::cout << FRED("[Error] Can't use address.") << '\n';
    return 1;
  }

  // Bind.
  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
  addr.sin_port = htons(PORT);
  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    std::cout << FRED("[Error] Can't bind socket to port.") << '\n';
    return 1;
  }

  // Make it nonblocking, and then listen.
  if (set_nonblocking(sock) == 1) {
    return 1;
  }
  if (listen(sock, SOMAXCONN) < 0) {
    std::cout << FRED("[Error] Error listening to socket.") << '\n';
    return 1;
  }

  // create the epoll socket
  int epoll_fd = epoll_create1(0);
  if (epoll_fd == -1) {
    std::cout << FRED("[Error] Error starting epoll.") << '\n';
    return 1;
  }

  // Mark the server socket for reading, and become edge-triggered.
  struct epoll_event event;
  memset(&event, 0, sizeof(event));
  event.data.fd = sock;
  event.events = EPOLLIN | EPOLLET;
  if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, sock, &event) == -1) {
    std::cout << FRED("[Error] Error setting socket for reading.") << '\n';
    return 1;
  }

  std::cout << KGRN << "Server started and listening on port " << PORT << RST << '\n';

  struct epoll_event *events = (epoll_event *) calloc(MAXEVENTS, sizeof(event));
  while (1) {
    int nevents = epoll_wait(epoll_fd, events, MAXEVENTS, -1);
    if (nevents == -1) {
      std::cout << FRED("[Error] Error on epoll_wait().") << '\n';
      return 1;
    }
    for (int i = 0; i < nevents; i++) {
      if ((events[i].events & EPOLLERR)
      || (events[i].events & EPOLLHUP)
      || (!(events[i].events & EPOLLIN))) {
        // error case
        std::cout << FRED("[Error] epoll error, ignoring.") << '\n';
        close(events[i].data.fd);
        continue;
      }
      else if (events[i].data.fd == sock) {
        // Someone is trying to connect, accept as many as we can.
        while(1) {
          struct sockaddr in_addr;
          socklen_t in_addr_len = sizeof(in_addr);
          int socket = accept(sock, &in_addr, &in_addr_len);
          if (socket == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
              // Processed all of the connections
              break;
            } else {
              std::cout << FRED("[Error] Failed to accept connection.") << '\n';
              return 1;
            }
          } else {
            // Simple accept connection, doesn't need to be processed by the
            // game loop.
            std::cout << KYEL << "Accepted new connection on fd " << socket << RST << '\n';
            set_nonblocking(socket);
            event.data.fd = socket;
            event.events = EPOLLIN | EPOLLET;
            if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, socket, &event) == -1) {
              std::cout << FRED("[Error] epoll_ctl()") << '\n';
              return 1;
            }
          }
        }
      }
      else {
        // Client socket; read as much data as we can.
        socket_buffer sb{events[i].data.fd, {}};

        while(1) {
          ssize_t nbytes = read(sb.socket, sb.buffer, sizeof(sb.buffer));
          if (nbytes == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
              // Finished reading data from socket.
              break;
            } else {
              std::cout << FRED("[Error] Error reading data from the socket, ignoring.") << '\n';
              break;
            }
          } else if (nbytes == 0) {
            std::cout << KYEL << "Socket disconnected " << sb.socket << RST << '\n';
            // Enqueue a packet crafted by the server.
            // 100 is the packet id for disconnect, must be set as a integer.
            socket_buffer disconnect_buffer{sb.socket, {0, 0, 0, 100}};
            queue.enqueue(disconnect_buffer);
            break;
          } else {
            // Push the data we received to the queue, to be processed by the
            // game loop.
            queue.enqueue(sb);
          }
        }
      }
    }
  }
  return 0;
}
