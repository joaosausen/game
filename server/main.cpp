// g++ main.cpp server.cpp system.cpp callbacks.cpp db.cpp -lpthread -Wall -std=c++17 -lsqlite3
#include <iostream>
#include <thread>
#include <bitset>

#include "entt/entt.hpp"
#include <sqlite3.h>
#include "readerwriterqueue/readerwriterqueue.h"
#include "readerwriterqueue/atomicops.h"

#include "db.hpp"
#include "system.hpp"
#include "components.hpp"
#include "colors.hpp"
#include "server.hpp"


/**
 * Server entry point. */
int main() {
  std::cout << FGRN("Starting server.") << '\n';

  // Queue, this is a message system to communicate the server with the game
  // logic. This is a FIFO and is thread safe.

  moodycamel::ReaderWriterQueue<socket_buffer> queue(100);

  // Start the server (sockets);
  std::thread server_t(start_server, std::ref(queue));

  // Init game logic.
  start_game_server(queue);

  return 0;
}
