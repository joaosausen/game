#pragma once
#include <string>
#include <vector>
#include <ctime>
#include <unordered_map>
#include <cmath>

#include "entt/entt.hpp"

// Forward declaration.
struct map_component;
void load_map(struct map_component &);

/** Definitions to be used in the registry context. */
// Lookup table to map socket (socket fd) to entities.
typedef std::unordered_map<int, entt::entity> socket_entity_map;

/** Represents a Map */
struct map_component {
  int id;
  std::string name;
  int tile_size;
  std::vector<std::vector<char>> tiles;

  map_component(int _id) {
    (*this).id = _id;
    load_map(*this);
  }
};

/** Stage holds everything on a map instance, shares an id with a map */
struct stage_component {
  std::time_t created = std::time(0);
  int online = 0;
};

/** An entity is anything. */
struct entity_component {
  int id_type;
  std::string name;
  float x; // Position.
  float y;
  entt::entity stage{entt::null}; // Reference.
};

struct movement_component {
  // Direction entity is moving, this is the sine and cosine values that will be
  // multiplied by velocity.
  float direction_x = 0.f;
  float direction_y = 0.f;
  float velocity = 0.f;
  float max_velocity = 100.f;
  float friction = 0.f;
};

/** Data related to the ball. */
struct ball_component {
  // Force to stop the ball.
  bool is_ball = true;
};

/**
 * A client is the connected user, it is created with a entity_component, so
 * their reference is share the same id.
 */
struct client_component {
  int socket;
};
