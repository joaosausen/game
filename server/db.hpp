/**
 * @file Database operations.
 *
 * This file contains code referenced to database persistence.
 */

#include <sstream>
#include <string>
#include <iostream>

#include "entt/entt.hpp"
#include <sqlite3.h>

#include "colors.hpp"

int setup_db(sqlite3 *);
int db_create_user(sqlite3 *, std::string, std::string);
int db_user_get_uid_from_login(sqlite3 *, std::string);
int db_try_login(sqlite3 *, std::string, std::string);
