#include <string>
#include <vector>
#include <chrono>
#include <iostream>
#include <unistd.h>

#include "colors.hpp"
#include "db.hpp"
#include "components.hpp"
#include "stage_manager.hpp"
#include "server.hpp"
#include "system.hpp"
#include "callbacks.hpp"
#include "map.hpp"

/**
 * Game tick.
 *
 * @param delta
 *  Elapsed time in microseconds. */
void tick(int idelta, entt::registry &registry, sqlite3 * db) {
  auto delta = 0.05f;
  /** Move entities. */
  registry.view<entity_component, movement_component>().each([&registry, delta](const auto entity, auto &ent, auto &mov) {
    if (mov.velocity > 0) {
      ent.x += mov.direction_x * mov.velocity * delta;
      ent.y += mov.direction_y * mov.velocity * delta;
    }

    // Craft packet and broadcast on stage. //@todo do not send it all the time.
    auto packet = packet_entity_move(entity, ent.x, ent.y, mov.direction_x, mov.direction_y, mov.velocity);
    stage_broadcast(registry, ent.stage, packet);

    if (mov.velocity > 0) {
      mov.velocity -= mov.friction;
    }
    if (mov.velocity < 0) {
      mov.velocity = 0;
    }
  });

  registry.view<entity_component, movement_component, ball_component>().each([&registry, delta](const auto entity, auto &ent, auto &mov, auto &bal) {
    // Check if ball is outside of field.
  });
}

/**
 * Manage packets, decode and call the callback based on the first integer
 * received. */
void packet_manager(socket_buffer sb, entt::registry &registry, sqlite3 * db) {
  // Extract first 4 bytes to identify the packet id.
  const auto socket = sb.socket;
  int packet_id = extract_from_buffer(sb, 0);
  std::cout << "[debug] packet_id: " << packet_id << '\n';
  const auto &ce_map = registry.ctx<socket_entity_map>();
  const auto socket_entity = ce_map.find(socket);
  entt::entity entity = entt::null;
  
  // Load if its in the lookup table.
  if (socket_entity != ce_map.end()) {
    entity = socket_entity->second;
  }

  switch (packet_id) {
    case 1: return request_move(sb, registry, entity); break;
    case 4: return request_login(sb, registry, db); break;
    case 5: return request_stage_list(sb, registry, entity); break;
    case 6: return request_select_stage(sb, registry, entity); break;
    case 7: return request_kick_ball(sb, registry, entity); break;
    case 100: return disconnect_socket(sb, registry); break;

    default:
      std::cout << KRED << "[Error] Invalid packet id " << packet_id << RST << '\n';
      break;
  }
}

/**
 * A stage is basically a room, for now there is no data on stages. */
void stage_create(entt::registry &registry) {
  // Create the stage.
  auto stage_entity = registry.create();
  registry.assign<stage_component>(stage_entity);
  registry.assign<map_component>(stage_entity, 1);

  // Create a ball associated with the stage.
  auto ball_entity = registry.create();
  registry.assign<ball_component>(ball_entity);
  registry.assign<entity_component>(ball_entity, 2, "Ball", 400.f, 400.f, static_cast<entt::entity>(stage_entity));
  registry.assign<movement_component>(ball_entity, 0.f, 0.f, 0.f, 300.f, 3.f);
};


/**
 * Start the registry, basically load content to the server.
 * This content, doesn't come from the database. @todo */
void setup_registry(entt::registry &registry) {
  // Create stages.
  stage_create(registry);
  stage_create(registry);
  stage_create(registry);
  stage_create(registry);
  stage_create(registry);
  std::cout << BOLD << KCYN << "5 stage(s) created." << RST << std::endl; // @todo
}

/**
 * Main game loop. */
int start_game_server(moodycamel::ReaderWriterQueue<socket_buffer> &queue) {

  // Build the registry and start some global variables.
  entt::registry registry;
  registry.set<socket_entity_map>();
  setup_registry(registry);

  // Start database.
  sqlite3 * db;
  if (sqlite3_open("game.db", &db)) {
    std::cout << "[Error] Can't open game database: " << sqlite3_errmsg(db) << '\n';
    return 0;
  }
  std::cout << FGRN("Database started.") << '\n';

  // Create tables on database if it doesn't exists.
  if (setup_db(db) == 1) {
    return 0;
  }

  /** Main game loop. */
  std::chrono::steady_clock::time_point begin;
  std::chrono::steady_clock::time_point end;
  int delta = 25;
  socket_buffer item;

  // Main loop.
  while (1) {
    // Process queue, no more than 50 items.
    // @todo better decide how to handle this, need to reserve some time.
    for (int i = 0; i < 50; i++) {
      if (queue.try_dequeue(item)) {
        packet_manager(item, registry, db);
      }
      else {
        break;
      }
    }

    // Run a tick.
    begin = std::chrono::steady_clock::now();
    tick(delta, registry, db);
    end = std::chrono::steady_clock::now();
    delta = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();

    // 20 FPS @todo make it run as needed.
    // 1 second = 1000000
    usleep(50000 - delta);
  }

  // When done.
  sqlite3_close(db);
  return 0;
}
