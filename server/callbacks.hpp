#pragma once

#include <sqlite3.h>
#include "entt/entt.hpp"

#include "components.hpp"
#include "server.hpp"

int _pack_float(float);
float _unpack_float(float);
std::string extract_string_from_buffer(const unsigned char, const int, const int);
void stage_broadcast(entt::registry &, entt::entity, Packet);
void packet_send(int, Packet);
Packet packet_create_entity(entt::entity, int, int, int, int);
Packet packet_entity_move(entt::entity, float, float, float, float, float);
Packet packet_delete_entity(entt::entity);
Packet packet_load_map(int);
Packet packet_popup_message(int, std::string);
Packet packet_stage_list(entt::registry &);
Packet packet_login_successful();
void disconnect_socket(socket_buffer, entt::registry &);
void setup_user(socket_buffer, entt::registry &, const int, entt::entity);
void request_move(socket_buffer, entt::registry &, entt::entity);
void request_login(socket_buffer, entt::registry &, sqlite3 *);
void request_stage_list(socket_buffer, entt::registry &, entt::entity);
void request_select_stage(socket_buffer, entt::registry &, entt::entity);
void request_kick_ball(socket_buffer, entt::registry &, entt::entity);
