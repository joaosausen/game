#pragma once

#include <fcntl.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <bitset>

#include "entt/entt.hpp"
#include "readerwriterqueue/readerwriterqueue.h"
#include "readerwriterqueue/atomicops.h"
#include <sqlite3.h>

#include "colors.hpp"
#include "components.hpp"

// This is for the data we received from a socket, this struct will be used in
// the moodycamel queue to pass data from one thread to another.
struct socket_buffer {
  int socket;
  unsigned char buffer[1024];
};

int set_nonblocking(int);
int start_server(moodycamel::ReaderWriterQueue<socket_buffer> &);
int extract_from_buffer(socket_buffer, const int);

class Packet {

  public:
    std::string buffer{};

    Packet &operator<<(const std::string data);
    Packet &operator<<(const int data);
    Packet &operator<<(const unsigned short data);
    Packet &operator<<(const unsigned char data);
    Packet &operator<<(const entt::entity data);
};
