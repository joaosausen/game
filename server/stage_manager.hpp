#pragma once

#include <string>
#include <vector>

#include "entt/entt.hpp"

#include "components.hpp"

/**
 * Create a stage.
 */
void stage_create(entt::registry &);
