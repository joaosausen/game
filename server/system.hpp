#pragma once

#include "entt/entt.hpp"
#include <sqlite3.h>
#include "readerwriterqueue/readerwriterqueue.h"
#include "readerwriterqueue/atomicops.h"
#include "server.hpp"

void tick(int, entt::registry &, sqlite3 *);
int start_game_server(moodycamel::ReaderWriterQueue<socket_buffer> &);
