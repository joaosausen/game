var EventManager = new class {
  constructor() {
    this.events = {};
  }

  listen(event, object) {
    let name = `${event}:${object.x},${object.y}`;
    if (!this.events[name]) {
      this.events[name] = [];
    }
    this.events[name].push(object);
  }

  stop_listening(event, object) {
    let name = `${event}:${object.x},${object.y}`;
    if (this.events[name]) {
      for (let i in this.events[name]) {
        if (this.events[name][i] == object) {
          this.events[name].splice(i, 1);
        }
      }
    }
  }

  trigger(event, position) {
    let name = event;
    if (position) {
      name += `:${position.x},${position.y}`;
    }
    if (this.events[name]) {
      for (let object of this.events[name]) {
        object.trigger();
      }
    }
  }
  
}();
