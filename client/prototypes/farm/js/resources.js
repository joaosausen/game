const _ItemPrices = {
  'PressurePlate': {money: 100},
  'Sprinkler': {money: 100},
  'Timer': {money: 100},
  'CartMoverStopper': {money: 50},
  'CartDisassembler': {money: 100},
  'CartReverser': {money: 100},
  'WaterDetector': {money: 200},
  'Locomotive': {money: 700},
  'Track': {money: 20},
  'Wagon': {money: 300},
  'WaterWagon': {money: 300},
  'Avocado': {money: 10},
  'Cassava': {money: 10},
  'Coffee': {money: 10},
  'Corn': {money: 10},
  'Cucumber': {money: 10},
  'Eggplant': {money: 10},
  'Grapes': {money: 10},
  'Lemon': {money: 10},
  'Melon': {money: 10},
  'Orange': {money: 10},
  'Pineapple': {money: 10},
  'Potato': {money: 10},
  'Rice': {money: 10},
  'Rose': {money: 10},
  'Strawberry': {money: 10},
  'Sunflower': {money: 10},
  'Tomato': {money: 10},
  'Tulip': {money: 10},
  'Turnip': {money: 10},
  'Wheat': {money: 10},
}
class ResourcesSystem extends SystemInterface {
  constructor(width, height) {
    super();
    this.width = width;
    this.height = height;
    this.reset();
  }

  reset() {
    this.resources = {
      money: 1000,
    };    
  }

  // Add or remove resources if negative.
  add(resources) {
    for (let resource in resources) {
      if (typeof this.resources[resource] == 'undefined') {
        return;
      }
      this.resources[resource] += resources[resource];
    }
  }

  subtract(resources) {
    for (let resource in resources) {
      if (typeof this.resources[resource] == 'undefined') {
        return;
      }
      this.resources[resource] -= resources[resource];
    }
  }

  // Like buy but for using resources directly.
  has(resources) {
    for (let resource in resources) {
      let quantity = resources[resource];
      if (!this.resources[resource] || quantity > this.resources[resource]) {
        return false;
      }
    }
    // Subtract.
    return true;
  }

  buy(item) {
    const resources_needed = this.get_price(item);
    if (resources_needed && this.has(resources_needed)) {
      this.subtract(resources_needed);
      return true;
    }
    return false;
  }

  can_buy(item) {
    const price = this.get_price(item);
    return price && this.has(price);
  }
  
  get_price(item) {
    return _ItemPrices[item] || null;
  }
};
