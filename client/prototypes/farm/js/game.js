/**
 * Save water and resources on save/load
 * use events to trigger elements, tick() will run later for cleanup.
 * Sprinklers and directional sprinklers.
 * Pick position on canvas and populate select.
 * fire triggers
 * fill sprinklers with alcohol or oil
 * fire lamps that put fire if there is alcohol on the ground
 * curvas em seções +
 * fix dead crops tiles
 * make it so we can connect locomotives on any place and pointing to any directions, and they can pull other wagons/locomotives from behind
*/

/**
 * https://en.wikipedia.org/wiki/Switcher
 */

// Setup register.

var Grid = [];
const width = 30;
const height = 30;
var Global = {
  turns: 0,
  tool: '',
};

var Crops = new CropsSystem(width, height);
var Mechanics = new MechanicsSystem(width, height);
var Trains = new TrainsSystem(width, height);
var Ground = new GroundSystem(width, height);
var Effects = new EffectsSystem(width, height);
var Resources = new ResourcesSystem(width, height);
var Enemies = new EnemiesSystem(width, height);
var ui = new Vue({ el: '#ui' });
var ui_top = new Vue({ el: '#ui-top' });

// Setup Canvas.
const CANVAS = document.getElementById("canvas");
const CONTEXT = CANVAS.getContext("2d");
CANVAS.width = width * 32;
CANVAS.height = height * 32;

function tick() {
  Global.turns++;
  Crops.tick();
  Trains.tick();
  Mechanics.tick();
  Ground.tick();
  Enemies.tick();
  ui.$refs.ui.tick();

  Mechanics.post_tick();
  Ground.post_tick();
}

function effects_tick() {
  Effects.tick();
}

setInterval(function() {
  effects_tick();
}, 100);

// Render items here instead of using systems, so we can control render order.
function render() {
  // Clear screen.
  CONTEXT.fillStyle = '#846a5c';
  CONTEXT.fillRect(0, 0, CANVAS.width, CANVAS.height);

  Ground.render();
  Crops.render();

  //Mechanics.cart_mover_stoppers.forEach((i) => { i.render(); });
  for (let y in Trains.tracks) {
    for (let x in Trains.tracks[y]) {
      if (Trains.tracks[y][x]) {
        Trains.tracks[y][x].render();
      }
    }
  }

  Mechanics.items['CartMoverStopper'].forEach((i) => { i.render(); });
  Mechanics.items['CartReverser'].forEach((i) => { i.render(); });
  Mechanics.items['PressurePlate'].forEach((i) => { i.render(); });
  Mechanics.items['CartDisassembler'].forEach((i) => { i.render(); });
  Mechanics.items['Timer'].forEach((i) => { i.render(); });
  Mechanics.items['Sprinkler'].forEach((i) => { i.render(); });
  Mechanics.items['WaterDetector'].forEach((i) => { i.render(); });
  Enemies.enemies.forEach((i) => { i.render(); });
  Trains.carts.forEach((i) => { i.render(); });
  Effects.render();
  ui.$refs.ui.render();

  // Render UI.
  let tile = get_mouse_tile();
  CONTEXT.fillStyle = "rgba(255, 0, 0, 0.2)";
  CONTEXT.fillRect(tile.x * 32, tile.y * 32, 32, 32);
  CONTEXT.fillStyle = '#fff';
  CONTEXT.font = "12px Arial";
  CONTEXT.fillText(`${tile.x} ${tile.y}`, 2, 12);

  requestAnimationFrame(render);
}

function canvas_click() {
  let mouse = get_mouse_tile();
  if (typeof window.tool == 'function') {
    window.tool(mouse);
  }
  switch (window.tool) {
    case 'crop_avocado': Crops.plant(mouse, Avocado); break;
    case 'crop_cassava': Crops.plant(mouse, Cassava); break;
    case 'crop_coffee': Crops.plant(mouse, Coffee); break;
    case 'crop_corn': Crops.plant(mouse, Corn); break;
    case 'crop_cucumber': Crops.plant(mouse, Cucumber); break;
    case 'crop_eggplant': Crops.plant(mouse, Eggplant); break;
    case 'crop_grapes': Crops.plant(mouse, Grapes); break;
    case 'crop_lemon': Crops.plant(mouse, Lemon); break;
    case 'crop_melon': Crops.plant(mouse, Melon); break;
    case 'crop_orange': Crops.plant(mouse, Orange); break;
    case 'crop_pineapple': Crops.plant(mouse, Pineapple); break;
    case 'crop_potato': Crops.plant(mouse, Potato); break;
    case 'crop_rice': Crops.plant(mouse, Rice); break;
    case 'crop_rose': Crops.plant(mouse, Rose); break;
    case 'crop_strawberry': Crops.plant(mouse, Strawberry); break;
    case 'crop_sunflower': Crops.plant(mouse, Sunflower); break;
    case 'crop_tomato': Crops.plant(mouse, Tomato); break;
    case 'crop_tulip': Crops.plant(mouse, Tulip); break;
    case 'crop_turnip': Crops.plant(mouse, Turnip); break;
    case 'crop_wheat': Crops.plant(mouse, Wheat); break;

    case 'remove_crop':
      if (Crops.grid[mouse.y] && Crops.grid[mouse.y][mouse.x]) {
        Crops.grid[mouse.y][mouse.x] = null;
      }
      break;

    // Tracks.
    case 'track': Trains.create_track(mouse); break;
    case 'track_bottom_left': Trains.create_track(mouse, ['bottom', 'left']); break;
    case 'track_left': Trains.create_track(mouse, ['left']); break;
    case 'track_right_bottom': Trains.create_track(mouse, ['right', 'bottom']); break;
    case 'track_right': Trains.create_track(mouse, ['right']); break;
    case 'track_top_left': Trains.create_track(mouse, ['top', 'left']); break;
    case 'track_top_right_bottom_left': Trains.create_track(mouse, ['right', 'bottom', 'left']); break;
    case 'track_bottom': Trains.create_track(mouse, ['bottom']); break;
    case 'track_right_left': Trains.create_track(mouse, ['right', 'left']); break;
    case 'track_top_bottom': Trains.create_track(mouse, ['top', 'bottom']); break;
    case 'track_top': Trains.create_track(mouse, ['top']); break;
    case 'track_top_right': Trains.create_track(mouse, ['top', 'right']); break;
    case 'track_right_bottom_left__bottom_left': Trains.create_track(mouse, ['right', 'bottom', 'left']).rotate(); break;
    case 'track_right_bottom_left__right_bottom': Trains.create_track(mouse, ['right', 'bottom', 'left']); break;
    case 'track_top_bottom_left__bottom_left': Trains.create_track(mouse, ['top', 'bottom', 'left']); break;
    case 'track_top_bottom_left__top_left': Trains.create_track(mouse, ['top', 'bottom', 'left']).rotate(); break;
    case 'track_top_right_bottom__right_bottom': Trains.create_track(mouse, ['top', 'right', 'bottom']).rotate(); break;
    case 'track_top_right_bottom__top_right': Trains.create_track(mouse, ['top', 'right', 'bottom']); break;
    case 'track_top_right_left__top_left': Trains.create_track(mouse, ['top', 'right', 'left']).rotate(); break;
    case 'track_top_right_left__top_right': Trains.create_track(mouse, ['top', 'right', 'left']); break;

    case 'remove_track': Trains.delete_track(mouse); break;

    case 'locomotive': Trains.create_cart(mouse, Locomotive); break;
    case 'wagon': Trains.create_cart(mouse, Wagon); break;
    case 'water_wagon': Trains.create_cart(mouse, WaterWagon); break;
    case 'remove_cart': Trains.delete(mouse, 'carts'); break;
    case 'cart_reverser': Mechanics.create(mouse, CartReverser); break;
    case 'remove_cart_reverser': Mechanics.delete(mouse, 'CartReverser'); break;
    case 'cart_disassembler': Mechanics.create(mouse, CartDisassembler); break;
    case 'remove_cart_disassembler': Mechanics.delete(mouse, 'CartDisassembler'); break;
    case 'timer': Mechanics.create(mouse, Timer); break;
    case 'remove_timer': Mechanics.delete(mouse, 'Timer'); break;
    case 'sprinkler': Mechanics.create(mouse, Sprinkler); break;
    case 'remove_sprinkler': Mechanics.delete(mouse, 'Sprinkler'); break;

    case 'water_detector': Mechanics.create(mouse, WaterDetector); break;
    case 'remove_water_detector': Mechanics.delete(mouse, 'WaterDetector'); break;
    
    case 'watering_can': Ground.get_ground(mouse).water(50); break;
    case 'scythe': Crops.harvest(mouse); break;
  }
}

function canvas_wheel(event) {
  Trains.canvas_wheel(event.deltaY);
}

function skip_turns(turns) {
  while (turns > 0) {
    turns--;
    tick();
  }
}
