class CropsSystem extends SystemInterface {
  constructor(width, height) {
    super();
    this.grid = [];
    for (let y = 0; y < height; y++) {
      this.grid[y] = [];
      for (let x = 0; x < width; x++) {
        this.grid[y][x] = null;
      }
    }
  }

  reset() {
    for (let y in this.grid) {
      for (let x in this.grid[y]) {
        this.grid[y][x] = null;
      }
    }
  }

  tick() {
    for (let y in this.grid) {
      for (let x in this.grid[y]) {
        if (this.grid[y][x]) {
          let keep = this.grid[y][x].tick();
          if (keep === false) {
            this.grid[y][x] = null;
          }
        }
      }
    }
  }

  render() {
    for (let y in this.grid) {
      for (let x in this.grid[y]) {
        if (this.grid[y][x]) {
          this.grid[y][x].render();
        }
      }
    }
  }

  harvest(position) {
    let crop = this.get_crop(position);
    if (crop) {
      if (crop.is_good_to_harvest()) {
        this.remove_crop(position);
        Resources.add({money: 50});
      }
      if (crop.is_dead()) {
        this.remove_crop(position);
      }
      add_text_sound_effect(position, 'wooosh');
    }
  }

  remove_crop(position) {
    delete(this.grid[position.y][position.x]);
  }

  plant(position, crop_type) {
    let type = class_to_string(crop_type);
    if (Resources.can_buy(type)) {
      if (!this.grid[position.y][position.x]) {
        let item = new crop_type(position.x, position.y, this);
        this.grid[position.y][position.x] = item;
        Resources.buy(type);
        return item;
      }
    }
    return null;
  }

  get_crop(position) {
    return this.grid[position.y] && this.grid[position.y][position.x] ? this.grid[position.y][position.x] : null;
  }

};

class CropBase {

  constructor(x, y, system) {
    this.x = x;
    this.y = y;
    this.system = system;
    this.turn = 0;
    this.phase = 0;
    Object.assign(this, Renderable(this));
    this.turns_to_die = 500;
  }

  update_image() {
    if (this.phases[this.phase]) {
      const image = 'crops/crop_' + this.image_base + (this.phase + 1) + '.png';
      this.load_image(image);
    }
    else {
      this.load_image('crops/' + this.dead_image);
    }
  }

  tick() {
    if (!this.phases[this.phase]) {
       // Dead.
      return;
    }

    // Only grow if there is water there.
    let phase_turns = this.phases[this.phase];
    let water_on_tile = Ground.get_water(this);
    if (water_on_tile > 0) {
      this.turn++;

      // Change phase.
      if (this.turn >= phase_turns) {
        this.turn = 0;
        this.phase++;
        this.update_image();
      }
    }
    else {
      this.turns_to_die--;
      if (this.turns_to_die < 0) {
        // Kill the plant, go to a index that doesn't exists.
        this.phase = this.phases.length;
        this.update_image();
      }
    }
  }

  is_good_to_harvest() {
    return this.phase == this.phases.length - 1;
  }

  is_dead() {
    return this.phase >= this.phases.length;
  }

  destroy() {
    //console.log('deleting item');
  }

}

class Avocado extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'avocado';
    this.name = 'Avocado';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Cassava extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'cassava';
    this.name = 'Cassava';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Coffee extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'coffee';
    this.name = 'Coffee';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Corn extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'corn';
    this.name = 'Corn';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Cucumber extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'cucumber';
    this.name = 'Cucumber';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Eggplant extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'eggplant';
    this.name = 'Eggplant';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Grapes extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'grapes';
    this.name = 'Grapes';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Lemon extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'lemon';
    this.name = 'Lemon';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Melon extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'melon';
    this.name = 'Melon';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Orange extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'orange';
    this.name = 'Orange';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Pineapple extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'pineapple';
    this.name = 'Pineapple';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Potato extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'potato';
    this.name = 'Potato';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Rice extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'rice';
    this.name = 'Rice';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Rose extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'rose';
    this.name = 'Rose';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Strawberry extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'strawberry';
    this.name = 'Strawberry';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Sunflower extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'sunflower';
    this.name = 'Sunflower';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Tomato extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'tomato';
    this.name = 'Tomato';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Tulip extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'tulip';
    this.name = 'Tulip';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Turnip extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'turnip';
    this.name = 'Turnip';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}

class Wheat extends CropBase {
  constructor(x, y, system) {
    super(...arguments);
    this.phases = [100, 100, 100, 100, 500];
    this.image_base = 'wheat';
    this.name = 'Wheat';
    this.dead_image = 'crop_dead1.png';
    this.update_image();
  }
}
