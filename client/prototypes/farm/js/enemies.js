function create_grid_2d(width, height, base_value) {
  let grid = [];
  for (let y = 0; y < height; y++) {
    grid[y] = [];
    for (let x = 0; x < width; x++) {
      grid[y][x] = base_value;
    }
  }
  return grid;
}

function vec2d_to_key(vec) {
  return `${vec.x}:${vec.y}`;
}

function key_to_vec2d(key) {
  let [x,y] = key.split(':');
  return {x, y};
}

function vec2d_copy(vec) {
  return {x: vec.x, y: vec.y};
}

class EnemiesSystem extends SystemInterface {
  constructor(width, height) {
    super();
    this.width = width;
    this.height = height;
    this.enemies = [];
    this.grid = create_grid_2d(width, height, 1);
  }

  reset() {
    this.enemies = [];
  }

  tick() {
    this.enemies.forEach((enemy) => { enemy.tick(); });
  }

  post_tick() {
    this.enemies.forEach((enemy) => { enemy.post_tick(); });
  }

  delete(position) {

  }

  create(position, type) {
    let enemy = new type(position.x, position.y, this);
    this.enemies.push(enemy);
  }

  get(position, container) {
    for (let i in container) {
      let item = container[i];
      if (is_equal2d(position, item)) {
        return item;
      }
    }
    return null;
  }

};

class EnemyBase {
  constructor(x, y, system) {
    this.x = x;
    this.y = y;
    this.system = system;

    // A grid representing where it is safe or unsafe to move.
    this.safe_grid = create_grid_2d(this.system.width, this.system.height, 0);

    // How far this enemy sight go.
    this.vision = 5;
    this.actions = [];

    // Add compositions.
    Object.assign(this, AIConditions(this), Renderable(this));
  }

  tick() {}
  post_tick() {}
}

class Gnome extends EnemyBase {
  static title = "Gnome";

  constructor(x, y, system) {
    super(...arguments);
    this.accumulator = 0;
    this.load_image('enemy_gnome.png');
    this.system = system;

    // Gnomes like to steal crops, this is a list of crops he knows about keyed
    // by position.
    this.crops = {};
    this.stolen_items = 0;
  }

  tick() {
    // Process actions.
    if (this.actions.length) {
      let action = this.actions[0];
      let done = action.tick();
      if (done) {
        this.actions.shift();
      }
    }
    else {
      this.make_plan();
    }

    // Look around.
    let crops = Crops.grid;
    let sight = 5;
    for (let y = this.y - sight; y <= this.y + sight; y++) {
      for (let x = this.x - sight; x <= this.x + sight; x++) {
        if ((y >= 0 && y < this.system.height)
        && (x >= 0 && x < this.system.width)) {
          let crop = Crops.grid[y][x];
          if (crop && !crop.is_dead()) {
            this.crops[vec2d_to_key(crop)] = crop.name;
          }
          else {
            // Delete in case there was a crop saved here.
            delete(this.crops[vec2d_to_key(vec2d(x, y))]);
          }
        }
      }
    }    
  }

  // Enemy got attacked.
  attacked() {
    console.log('run away');
  }

  make_plan() {
    // Check items close.
    const crop_pos = this._get_crop_close_pos(5);
    const has_crop_close = !!crop_pos;
        
    if (this._in_danger()) {
      console.log('run away');
      return;
    }

    if (has_crop_close) {
      this.actions.push(new MoveAction(this, crop_pos));
      this.actions.push(new HarvestCropAction(this, crop_pos));
      this.actions.push(new IdleAction(this, 5));
      return;
    }
    
    return this.actions.push(new IdleAction(this));
    


    if (!this._know_the_terrain()) {
      console.log('scan the terrain');
      return;
    }
    if (!this._get_crop_close_pos()) {
      console.log('move to a safe place and scan around');
      if (!this._can_move_safely()) {
        console.log('wait');
      }
    }
    else {
      console.log('move to');
      console.log('steal');
    }
    
  }
  
}

Reflection.register(Gnome);


function gnome() {
  Enemies.create(vec2d(10, 10), Gnome);
}
