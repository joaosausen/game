function direction_to_2d(direction) {
  const dirs = {
    top: {y: -1, x: 0},
    right: {y: 0, x: 1},
    bottom: {y: 1, x: 0},
    left: {y: 0, x: -1}
  };
  return dirs[direction];
}

function _2d_to_direction(position) {
  const dirs = {
    top: {y: -1, x: 0},
    right: {y: 0, x: 1},
    bottom: {y: 1, x: 0},
    left: {y: 0, x: -1}
  };
  for (let p in dirs) {
    if (dirs[p].y == position.y && dirs[p].x == position.x) {
      return p;
    }
  }
}

function invert_direction(direction) {
  const dirs = {
    top: 'bottom',
    right: 'left',
    bottom: 'top',
    left: 'right',
  };
  return dirs[direction];
}

function get_neighbor_positions(position) {
  return {
    top: sum2d(position, {x: 0, y:-1}),
    right: sum2d(position, {x: 1, y: 0}),
    bottom: sum2d(position, {x: 0, y: 1}),
    left: sum2d(position, {x: -1, y: 0}),
  }
}

function get_directions() {
  return ['top', 'right', 'bottom', 'left'];
}

function is_locomotive(cart) {
  return (cart && (cart instanceof Locomotive));
}

function is_main_locomotive(cart) {
  return cart && cart.front == null && is_locomotive(cart);
}

function is_last_cart(cart) {
  return cart && !cart.back;
}

function get_first_cart(cart) {
  let iterator = cart;
  while (iterator.front) {
    iterator = iterator.front;
  }
  return iterator;
}

function get_last_cart(cart) {
  let iterator = cart;
  while (iterator.back) {
    iterator = iterator.back;
  }
  return iterator;
}

class TrainsSystem extends SystemInterface {
  constructor(width, height) {
    super();
    this.tracks = [];
    for (let y = 0; y < height; y++) {
      this.tracks[y] = [];
      for (let x = 0; x < width; x++) {
        this.tracks[y][x] = null;
      }
    }
    this.carts = [];
  }

  reset() {
    for (let y in this.tracks) {
      for (let x in this.tracks[y]) {
        this.tracks[y][x] = null;
      }
    }
    this.carts = [];
  }

  tick() {
    // Process locomotives only (first car).
    let locomotives = this.carts.filter((cart) => {
      return is_main_locomotive(cart);
    });
    for (let i in locomotives) {
      locomotives[i].tick();
    }
  }

  post_tick() {
    let locomotives = this.carts.filter((cart) => {
      return is_main_locomotive(cart);
    });
    for (let i in locomotives) {
      locomotives[i].post_tick();
    }
  }

  canvas_wheel(y) {
    let mouse = get_mouse_tile();
    let cart = this.get_cart(mouse);
    let dir = y > 0 ? 1 : -1;
    if (cart) {
      cart.rotate(dir);
    }
    else {
      let track = this.get_track(mouse);
      if (track) {
        track.rotate(dir);
      }
    }
  }

  create_cart(position, cart_type = Locomotive) {
    let type = class_to_string(cart_type);
    if (!this.get_cart(position)) {
      if (Resources.can_buy(type)) {
        let cart = new cart_type(position.x, position.y, this);
        this.carts.push(cart);
        Resources.buy(type);
        return cart;
      }
    }
    return null;
  }

  delete_track(position) {
    if (Trains.tracks[position.y] && Trains.tracks[position.y][position.x]) {
      Trains.tracks[position.y][position.x] = null;
      let price = Resources.get_price('Track');
      Resources.add(price);
      let track = Trains.get_track(sum2d(position, {x: 0, y:-1}));
      if (track) { track.update(); }
      track = Trains.get_track(sum2d(position, {x: 1, y: 0}));
      if (track) { track.update(); }
      track = Trains.get_track(sum2d(position, {x: 0, y: 1}));
      if (track) { track.update(); }
      track = Trains.get_track(sum2d(position, {x:-1, y: 0}));
      if (track) { track.update(); }
    }
  }

  delete(position, container) {
    let i = null;
    for (i in this[container]) {
      if (is_equal2d(this[container][i], position)) {
        let type = this[container][i].constructor.name;
        let price = Resources.get_price(type);
        Resources.add(price);
        if (typeof this[container][i].destroy === 'function') {
          this[container][i].destroy();
        }
        delete(this[container][i]);
        this[container].splice(i, 1);
        break;
      }
    }
  }

  create_track(position, connections = []) {
    if (Resources.can_buy('Track')) {
      if (this.tracks[position.y][position.x]) {
        delete(this.tracks[position.y][position.x]);
      }
      else {
        Resources.buy('Track');
      }
      let item = new Track(position.x, position.y, this, connections);
      this.tracks[position.y][position.x] = item;
      // Refresh neighbors.
      let neighbors = get_neighbor_positions(item);
      for (let i in neighbors) {
        let track = this.get_track(neighbors[i]);
        if (track) {
          track.update();
        }
      }
      return item;
    }
    return null;
  }

  get_track(position) {
    return this.tracks[position.y] && this.tracks[position.y][position.x] ? this.tracks[position.y][position.x] : null;
  }

  get(position, container) {
    for (let i in container) {
      let item = container[i];
      if (is_equal2d(position, item)) {
        return item;
      }
    }
    return null;
  }

  get_cart(position) {
    return this.get(position, this.carts);
  }

};

class Track {
  static title = "Track";

  constructor(x, y, system, connections = []) {
    Object.assign(this, Renderable(this));
    this.x = x;
    this.y = y;
    this.system = system;
    // If we used the auto track tool, it will auto connect, otherwise it will
    // force to not auto connect. Connections is array with all directions it
    // can connect.
    this.auto = !connections.length;
    this.connections = {top: true, right: true, bottom: true, left: true};

    if (!this.auto) {
      const directions = get_directions();
      for (let i in directions) {
        let direction = directions[i];
        this.connections[direction] = connections.includes(direction);
      }
    }
    this.update();
  }

  serialize() {
    return {
      x: this.x,
      y: this.y,
      connections: this.connections,
      auto: this.auto,
      direction: this.direction,
      directions: this.directions,
    };
  }

  update() {
    // Neighbors, check if there is a track on that position then check if that
    // track connects to the inverted direction, in this case, connect to this.
    const directions = get_directions();
    for (let d in directions) {
      let direction = directions[d];
      let direction_position = sum2d(this, direction_to_2d(direction));
      let track = this.system.get_track(direction_position);
      // If there is no track or we don't connect to that direction.
      if (!track || !this.connections[direction]) {
        this[direction] = false;
      }
      else {
        // We can connect to that direction, but lets check if the track on that
        // direction connects to us.
         let track_connects = track.connections[invert_direction(direction)];
         this[direction] = track_connects ? direction_position : false;
      }
    }

    // Set the image.
    let name = ['track'];
    this.sections = 0;

    // If its a auto track, use the calculated this.top, this.left, this.right
    // and this.bottom values, otherwise use the values hardcoded on
    // this.connections.
    const container = this.auto ? this : this.connections;
    if (container.top) { name.push('top'); this.sections++; }
    if (container.right) { name.push('right'); this.sections++; }
    if (container.bottom) { name.push('bottom'); this.sections++; }
    if (container.left) { name.push('left'); this.sections++; }
    name = name.join('_');

    // Directions are connections.
    this.directions = [];
    if (container.top && container.right) { this.directions.push('top_right'); }
    if (container.right && container.bottom) { this.directions.push('right_bottom'); }
    if (container.bottom && container.left) { this.directions.push('bottom_left'); }
    if (container.top && container.left) { this.directions.push('top_left'); }
    if (container.top && container.bottom && this.sections < 3) { this.directions.push('top_bottom'); }
    if (container.right && container.left && this.sections < 3) { this.directions.push('right_left'); }

    if (!this.directions.includes(this.direction)) {
      if (this.sections == 4) {
        this.direction = 'top_bottom';
      }
      else {
        // Get a default direction.
        const direction = this.directions.length ? this.directions[0] : null;
        this.set_direction(direction);
      }
    }

    // If on a T or cross section, need to add a direction to the name of the
    // image indicating where its going to.
    if (this.sections == 3) {
      if (this.direction) {
        name = name + '__' + this.direction;
      }
    }
    name += '.png';
    this.load_image(name);
  }

  neighbors() {
    return [
      this.top, // top
      this.right, // right
      this.bottom, // bottom
      this.left, // left
    ].filter((item) => {
      return item;
    });
  }

  trigger() {
    this.rotate();
  }

  rotate(direction = 1) {
    let index = this.directions.indexOf(this.direction);
    if (index < 0) { // Error.
      this.direction = this.directions[0];
    }
    index += direction + this.directions.length;
    index = index % this.directions.length;
    this.set_direction(this.directions[index]);
    this.update();
  }

  set_direction(direction) {
    if (this.directions.includes(direction)) {
      this.direction = direction;
    }
  }

  // Cart enters in a direction and might leave in a different direction.
  next(cart_direction) {
    // Can't do curves on + sections for now.
    if (this.direction && this.sections < 4) {
      // This means that if a cart is moving "top" he should match a *_bottom
      // direction.
      let inverted = invert_direction(cart_direction);
      const [a, b] = this.direction.split('_');
      if (inverted == a) { return b; }
      if (inverted == b) { return a; }
    }
    let cart_direction_2d = direction_to_2d(cart_direction);
    let next_straight_track = sum2d(cart_direction_2d, this);
    if (this.system.get_track(next_straight_track)) {
      return cart_direction;
    }
    // Stop.
    return null;
  }

}

/** Carts */
class CartBase {
  constructor(x, y, system) {
    Object.assign(this, Renderable(this));
    this.x = x;
    this.y = y;
    this.system = system;
    this.direction = null;
    // Locomotive/Wagon in front/back to this Locomotive/Wagon.
    this.back = null;
    this.front = null;

    let track = this.system.get_track(this);
    if (track) {
      if (track.direction) {
        this.direction = track.direction.split('_').pop();
      }
    }
    this.direction = this.direction ?? 'left';
    this.try_connect();
  }

  post_tick() { }

  try_connect() {
    let neighbors = get_neighbor_positions(this);
    let inverted_direction = invert_direction(this.direction);

    // Try to connect to carts next to it.
    if (!this.back) {
      let cart_behind = this.system.get_cart(neighbors[inverted_direction]);
      if (!cart_behind) {
        // Check for carts on his side.
        if (this.direction == 'top' || this.direction == 'bottom') {
          cart_behind = this.system.get_cart(neighbors['right']);
          if (!cart_behind) {
            cart_behind = this.system.get_cart(neighbors['left']);
          }
        }
        else {
          cart_behind = this.system.get_cart(neighbors['top']);
          if (!cart_behind) {
            cart_behind = this.system.get_cart(neighbors['bottom']);
          }
        }
      }
      if (cart_behind && (is_locomotive(this) || !(is_locomotive(cart_behind)))) {
        this.set_back(cart_behind);
      }
    }
    if (!this.front) {
      let cart_in_front = this.system.get_cart(neighbors[this.direction]);
      if (cart_in_front) {
        if (is_locomotive(this)) {
          if (is_locomotive(cart_in_front)) {
            this.set_front(cart_in_front);
          }
        }
        else {
          this.set_front(cart_in_front);
        }
      }
      else {
        let cart_behind = this.system.get_cart(neighbors[inverted_direction]);
        if (cart_behind) {
          if (!(is_locomotive(this))) {
            this.set_front(cart_behind);
          }
        }
      }
    }
  }

  update_image() {
    const image = `cart_${this.image_base}_${this.direction}.png`;
    this.load_image(image);
  }

  // Move the cart in the back and update it before pushing the next cart.
  push(x, y, direction) {
    if (this.back) {
      const original_direction = this.back.direction;
      const original_x = this.back.x;
      const original_y = this.back.y;
      this.back.x = x;
      this.back.y = y;
      this.back.direction = direction;
      this.back.update_image();
      this.back.tick();
      this.back.push(original_x, original_y, original_direction);
    }
  }

  set_front(position) {
    let cart = this.system.get_cart(position);
    if (cart.back == null && cart != this.back) {
      this.front = cart;
      cart.back = this;
      this.set_direction();
      add_text_sound_effect(this, 'clang!');
    }
    return this;
  }

  set_back(position) {
    let cart = this.system.get_cart(position);
    if (cart.front == null && cart != this.front) {
      this.back = cart;
      cart.front = this;
      cart.set_direction();
      add_text_sound_effect(this, 'clang!');
    }
    return this;
  }

  tick() { }

  set_direction(direction = null) {
    if (!direction) {
      // Auto point to next cart.
      if (this.front) {
        let direction = _2d_to_direction(subtract2d(this.front, this));
        if (direction) {
          this.direction = direction;
        }
      }
      return this;
    }
    let directions = get_directions();
    if (directions.includes(direction)) {
      this.direction = direction;
    }
    return this;
  }

  rotate(direction = 1) {
    if (!this.moving) {
      let directions = get_directions();
      let index = directions.indexOf(this.direction);
      if (index < 0) { // Error.
        this.direction = this.directions[0];
      }
      index += direction + directions.length;
      index = index % directions.length;
      this.set_direction(directions[index]);
      this.update_image();
      this.try_connect();
    }
  }

  destroy() {
    if (this.front) {
      this.front.back = null;
      this.front = null;
    }
    if (this.back) {
      this.back.front = null;
      this.back = null;
    }
  }

}

class Locomotive extends CartBase {
  static title = "Locomotive";

  constructor(x, y, system) {
    super(x, y, system);
    this.moving = false;
    this.image_base = 'locomotive';
    this.update_image();
    // By default the train move forward.
    this.forward = true;
    this.backward_counter = 0;
  }

  tick() {
    if (!this.moving) {
      return;
    }

    const original_x = this.x;
    const original_y = this.y;
    if (this.forward) {
      this.backward_counter = 0;
      // Get track.
      let track = this.system.get_track(this);
      if (track) {
        let direction = track.next(this.direction);
        if (direction) {
          // Check if there is a cart on next track.
          let direction_2d = direction_to_2d(direction);
          let next_position = sum2d(direction_2d, this);
          let cart = this.system.get_cart(next_position);
          if (!cart) {
            // Move ahead then push back cart to our original position.
            const original_direction = this.direction;
            this.direction = direction;
            const next = sum2d(direction_to_2d(this.direction), this);
            this.x = next.x;
            this.y = next.y;
            this.push(original_x, original_y, original_direction);
          }
          else {
            this.stop();
          }
        }
      }
    }
    else {
      this.backward_counter++;
      if (this.backward_counter >= 2) {
        this.backward_counter = 0;
        // Moving backward.
        var last = get_last_cart(this);
        let backward_direction = invert_direction(last.direction);
        let last_track = this.system.get_track(last);
        // The train might be out of tracks.
        if (!last_track) {
          return;
        }
        let next_backward_direction = last_track.next(backward_direction);
        if (next_backward_direction) {
          let next_backward = sum2d(direction_to_2d(next_backward_direction), last);
          let first_run = true;
          let cart_backward = this.system.get_cart(next_backward);
          // If there is a cart behind it means we didn't connect to it, do not
          // move.
          if (!cart_backward) {
            let iteration_cart = last;
            do {
              let old_x = iteration_cart.x;
              let old_y = iteration_cart.y;
              iteration_cart.x = next_backward.x;
              iteration_cart.y = next_backward.y;
              next_backward.x = old_x;
              next_backward.y = old_y;
              iteration_cart.direction = _2d_to_direction(subtract2d(next_backward, iteration_cart));
              iteration_cart.update_image();
              iteration_cart = iteration_cart.front;
            } while (iteration_cart);
            // After moving, try to connect to wagon, if we connect then wait
            // one more turn.
            last.try_connect();
            if (last.back) {
              this.backward_counter--;
            }
          }
        }
      }
    }
    if (this.x != original_x || this.y != original_y) {
      EventManager.trigger('locomotive_move', this);
    }
    this.update_image();
  }

  move() {
    this.moving = true;
    this.forward = true;
    return this;
  }

  backward() {
    this.moving = true;
    this.forward = false;
    return this;
  }

  stop() {
    this.moving = false;
    return this;
  }
}
Reflection.register(Locomotive);

/** Carry items? */
class Wagon extends CartBase {
  static title = "Wagon";

  constructor(x, y, system) {
    super(x, y, system);
    this.image_base = 'wagon';
    this.update_image();
  }

  tick() {
  }
}
Reflection.register(Wagon);

class WaterWagon extends CartBase {
  static title = "Water wagon";

  constructor(x, y, system) {
    super(x, y, system);
    this.image_base = 'water_wagon';
    this.update_image();
  }

  tick() {
    Ground.water({x: this.x, y: this.y + 1});
    Ground.water({x: this.x, y: this.y - 1});
    Ground.water({x: this.x + 1, y: this.y});
    Ground.water({x: this.x - 1, y: this.y});
  }
}
Reflection.register(WaterWagon);
