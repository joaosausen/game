class EffectsSystem extends SystemInterface {
  constructor() {
    super();
    this.effects = [];
  }

  add_effect(effect) {
    this.effects.push(effect);
  }

  tick() {
    for (let i in this.effects) {
      let keep = this.effects[i].tick();
      if (keep === false) {
        this.effects.splice(i, 1);
      }
    }
  }

  render() {
    CONTEXT.font = "italic small-caps bold 12px Arial";
    for (let i in this.effects) {
      this.effects[i].render();
    }
  }
}


class EffectBase {
  tick() { };
  render() { };
}


class WaterEffect extends EffectBase {
  constructor(position) {
    super();
    this.x = position.x;
    this.y = position.y;
    this.turns_to_live = 12;
    this.update_counter = 3;
    this.update();
  }

  update() {
    if (this.update_counter == 3) {
      this.rain = {
        x: (this.x * 32) + (Math.random() * 32),
        y: (this.y * 32) + (Math.random() * 32)
      };
      this.update_counter = 0;
    }
    this.update_counter++;
  }

  tick() {
    if (this.turns_to_live <= 0) {
      return false;
    }
    this.update();
    this.turns_to_live--;
  }

  render() {
    const color = `rgba(0, 140, 160)`;
    CONTEXT.fillStyle = color;
    const symbol = [".·'", "'.", "..", "´.", "`.·"];
    const i = Math.floor(Math.random() * 5);
    CONTEXT.fillText(symbol[i], this.rain.x, this.rain.y); 
  }
}

class TextSoundEffect extends EffectBase {
  constructor(position, text, turns_to_live) {
    super();
    this.x = position.x;
    this.y = position.y;
    this.text = text;
    this.turns_to_live = turns_to_live * 10;
    this.turn_count = 0;
  }

  tick() {
    if (this.turn_count >= this.turns_to_live) {
      return false;
    }
    this.turn_count++;
  }

  render() {
    const transparency = 1 - (this.turn_count / this.turns_to_live);
    const color = `rgba(255, 255, 210, ${transparency})`;
    const offset = this.turn_count * -2;
    CONTEXT.fillStyle = color;
    CONTEXT.fillText(this.text, this.x * 32 + offset, this.y * 32 + offset); 
  }
}

class StaticTextEffect extends TextSoundEffect {
  constructor(position, text, turns_to_live) {
    super(position, text, turns_to_live);
  }

  render() {
    const transparency = 1 - (this.turn_count / this.turns_to_live);
    const color = `rgba(255, 255, 150, ${transparency})`;
    CONTEXT.fillStyle = color;
    CONTEXT.fillText(this.text, this.x * 32, this.y * 32); 
  }
}


function add_text_sound_effect(position, text, turns = 0.5) {
  let effect = new TextSoundEffect(position, text, turns);
  Effects.add_effect(effect);
}

function add_water_effect(position) {
  let effect = new WaterEffect(position);
  Effects.add_effect(effect);
}

function add_static_text_effect(position, text, turns = 1) {
  let effect = new StaticTextEffect(position, text, turns);
  Effects.add_effect(effect);
}
