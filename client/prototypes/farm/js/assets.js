var ASSETS = {};

function preload() {
  var assets = [
    // Tools.
    'tool_hoe.png', 'tool_scythe.png', 'tool_watering_can.png',
    // Tracks.
    'track_bottom_left.png', 'track_left.png', 'track_right_bottom.png',
    'track_right.png', 'track_top_left.png', 'track_top_right_bottom_left.png',
    'track_bottom.png', 'track.png', 'track_right_left.png', 'track_top_bottom.png',
    'track_top.png', 'track_top_right.png',
    'track_right_bottom_left__bottom_left.png', 'track_right_bottom_left__right_bottom.png',
    'track_top_bottom_left__bottom_left.png', 'track_top_bottom_left__top_left.png',
    'track_top_right_bottom__right_bottom.png', 'track_top_right_bottom__top_right.png',
    'track_top_right_left__top_left.png', 'track_top_right_left__top_right.png',
    // Ground.
    'wet_ground_bottom_left.png', 'wet_ground_left.png', 'wet_ground_right_bottom.png',
    'wet_ground_right.png', 'wet_ground_top_left.png', 'wet_ground_top_right_bottom_left.png',
    'wet_ground_bottom.png', 'wet_ground.png', 'wet_ground_right_left.png', 'wet_ground_top_bottom.png',
    'wet_ground_top.png', 'wet_ground_top_right.png',
    'wet_ground_right_bottom_left.png', 'wet_ground_top_bottom_left.png',
    'wet_ground_top_right_bottom.png', 'wet_ground_top_right_left.png',
    // Cart.
    'cart_locomotive_left.png', 'cart_locomotive_right.png', 'cart_locomotive_top.png', 'cart_locomotive_bottom.png',
    'cart_wagon_left.png', 'cart_wagon_right.png', 'cart_wagon_top.png', 'cart_wagon_bottom.png',
    'cart_water_wagon_left.png', 'cart_water_wagon_right.png', 'cart_water_wagon_top.png', 'cart_water_wagon_bottom.png',
    // Mechanics misc.
    'pressure_plate.png',
    'cart_mover_stopper_active.png', 'cart_mover_stopper_inactive.png',
    'cart_reverser.png',
    'cart_disassembler.png',
    'timer_inactive.png', 'timer_active1.png', 'timer_active2.png', 'timer_active3.png', 'timer_active4.png', 'timer_active0.png',
    'sprinkler_active.png', 'sprinkler_inactive.png',
    'water_detector.png',
    // Error.
    'missing_tile.png',
    // Enemies.
    'enemy_gnome.png',
  ];

  // Crops.
  const crops = [
    'avocado', 'cassava', 'coffee', 'corn', 'cucumber', 'eggplant', 'grapes',
    'lemon', 'melon', 'orange', 'pineapple', 'potato', 'rice', 'rose',
    'strawberry', 'sunflower', 'tomato', 'tulip', 'turnip', 'wheat',
  ];
  for (let c in crops) {
    assets.push('crops/crop_' + crops[c] + '.png');
    for (let i = 1; i <= 5; i++) {
      assets.push('crops/crop_' + crops[c] + i + '.png');
    }
  }

  for (let i = 0; i <= 9; i++) {
    assets.push('crops/crop_dead' + i + '.png');
  }
  
  for (let i in assets) {
    let asset = assets[i];
    ASSETS[asset] = new Image();
    ASSETS[asset].src = 'assets/' + asset;
    ASSETS[asset].onload = () => {
       // console.log(asset + ' loaded');
    };
  }
}

preload();
