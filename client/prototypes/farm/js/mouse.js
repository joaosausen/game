var MOUSE = {x: 0, y: 0};

function get_mouse() {
  return MOUSE;
}

function get_mouse_tile() {
  return {
    x: Math.floor(MOUSE.x / 32),
    y: Math.floor(MOUSE.y / 32)
  };
}

function set_mouse(event) {
  const rect = CANVAS.getBoundingClientRect();
  const scaleX = CANVAS.width / rect.width;
  const scaleY = CANVAS.height / rect.height;

  MOUSE = {
    x: parseInt((event.clientX - rect.left) * scaleX),
    y: parseInt((event.clientY - rect.top) * scaleY)
  };
}

CANVAS.onmousemove = (event) => {
  let old_tile = get_mouse_tile();
  set_mouse(event);
  if (event.buttons == 1) {
    let new_tile = get_mouse_tile();
    if (old_tile.x != new_tile.x || new_tile.y != old_tile.y) {
      canvas_click();
    }
  }
  
};

document.addEventListener('contextmenu', event => event.preventDefault());

CANVAS.onmousedown = (event) => {
  // Left click.
  if (event.buttons == 1) {
    canvas_click();
  }
  // Right click.
  if (event.buttons == 2) {
    //context_menu();
  }
};

CANVAS.onwheel = (event) => {
  event.preventDefault();
  canvas_wheel(event);
};
