function vec2d() {
  switch (arguments.length) {
    case 2:
      return {x: arguments[0], y: arguments[1]};
    case 1:
      return {x: arguments[0].x, y: arguments[0].y} 
  }
}

function distance(a, b) {
  return Math.abs(b.x - a.x) + Math.abs(b.y - a.y);
}

function astar_test() {
  let grid = [
    [1, 0, 1, 1],
    [1, 0, 0, 1],
    [1, 0, 1, 1],
    [1, 1, 1, 0]
  ];

  let result = astar(vec2d(0,0), vec2d(2,0), grid);
  for (let r of result) {
    //console.log(r.x, r.y);
    grid[r.y][r.x] = '!';
  }
  for (let y in grid) {
    console.log(...grid[y]);
  }
}

function astar(start, end, grid = null) {

  let heuristic = (a, b) => {
    return Math.abs(b.x - a.x) + Math.abs(b.y - a.y);
  };

  let is_in = (container, position) => {
    for (let item of container) {
      if (is_equal2d(item, position)) {
        return true;
      }
    }
    return false;
  };

  let is_passable = (grid, p) => {
    return grid[p.y] && grid[p.y][p.x] && grid[p.y][p.x] > 0;
  };
  
  // Build the map with information from systems.
  grid = grid || [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0]
  ];
  let openlist = [];
  let closedlist = [];

  // h = heuristics = estimated distance from this node to the end.
  // g = distance from this node to start
  // f = h + g
  start.g = 0;
  start.h = heuristic(start, end);
  start.f = start.g + start.h;
  start.parent = null;
  openlist.push(start);

  while (openlist.length > 0) {
    // Lowest heuristics.
    let lowest_f = 0;
    for (let i in openlist) {
      if (openlist[i].f < openlist[lowest_f].f) {
        lowest_f = i;
      }
    }
    let current = openlist[lowest_f];

    // Found a route.
    if (is_equal2d(current, end)) {
      let route = [];
      let curr = current;
      while (curr.parent) {
        route.push(vec2d_copy(curr));
        curr = curr.parent;
      }
      route.push(vec2d_copy(start));
      return route.reverse();
    }

    // Move from open to closed.
    closedlist.push(current);
    openlist.splice(lowest_f, 1);

    // Process neighbors.
    let neighbors = get_neighbor_positions(current);
    for (let i in neighbors) {
      let neighbor = neighbors[i];
      if (is_in(closedlist, neighbor) || !is_passable(grid, neighbor)) {
        // Processed already.
        continue;
      }

      // This 1 can be the terrain value, so a montain has a penalty over plain
      // grass.
      let g = current.g + 1;
      // G score from the current node is better to arrive to that node, in case
      // some other node arrived there already, we want the best distance from
      // start to this neighbor.
      let g_score_is_better = false;

      if (!is_in(openlist, neighbor)) {
        // First time here, since its not on the closed list and not on the open
        // list.
        g_score_is_better = true;
        neighbor.h = heuristic(neighbor, end);
        openlist.push(neighbor);
      }
      else if (g < neighbor.g) {
        // Already on the openlist, has a calculated g.
        g_score_is_better = true;
      }

      if (g_score_is_better) {
        neighbor.parent = current;
        neighbor.g = g;
        neighbor.f = neighbor.g + neighbor.h;
      }
    }

  }

  // No route found.
  return [];
}
