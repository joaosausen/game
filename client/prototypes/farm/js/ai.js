
// Conditions.
const AIConditions = (self) => ({
  
  // Entity is in danger.
  _in_danger: () => {
    return (self.safe_grid[self.y][self.x] >= 1);
  },

  // Entity can move to x/y safely, outside of towers ranges, etc.
  _can_move_safely: (position) => {
    return false;
  },

  _get_crop_close_pos: (max_distance) => {
    if (self.crops) {
      for (let key in self.crops) {
        let crops_pos = key_to_vec2d(key);
        if (distance(crops_pos, self) <= max_distance) {
          return crops_pos;
        }
      }
    }
    return false;
  },

  // Entity can sabotage target.
  _can_sabotage: (target) => {
    return false;
  },

  // Entity can steal target.
  _can_steal: (target) => {
    return false;
  },

  // Return true if the entity already scanned the terrain around.
  _know_the_terrain: () => {
    return false;
  },

});

// Actions.
class Action {
  tick() { }
}

// 
class MoveAction extends Action {
  // Route is an array of positions given by astar.
  constructor(entity, destination) {
    super();
    this.entity = entity;
    this.route = astar(this.entity, destination, this.entity.system.grid);
    // The first one is the entity position, we can remove it.
    if (this.route.length) { this.route.shift(); }
  }

  tick() {
    // No route or arrived already.
    if (!this.route.length) {
      return true;
    }

    let next = this.route.shift();
    this.entity.x = next.x;
    this.entity.y = next.y;
    return false;
  }
}

// Move to a safe position at the edge of where I know already.
class ExploreAction extends Action {
  constructor(entity) {
    super();
    this.entity = entity;
    console.log("ExploreAction");
  }
  tick() {

  }
}

class HarvestCropAction extends Action {
  constructor(entity, crop_position) {
    super();
    this.entity = entity;
    this.crop_position = crop_position;
    this.delay = 2;
    console.log('HarvestCropAction');
  }

  tick() {
    if (this.delay <= 0) {
      let crop = Crops.get_crop(this.crop_position);
      console.log(crop);
      if (crop) {
        if (!isNaN(this.entity.stolen_items)) {
          this.entity.stolen_items++;
        }
        Crops.remove_crop(this.crop_position);
        add_text_sound_effect(this.entity, '!!!');
      }
      else {
        add_text_sound_effect(this.entity, '???');
      }
      return true;
    }
    this.delay--;
    return false;
  }
}

// Do nothing for turns.
class IdleAction extends Action {
  constructor(entity, turns = 20, message = null) {
    super();
    this.entity = entity;
    this.accumulator = 0;
    this.turns = turns;
    this.message = message;
    console.log("IdleAction");
  }

  tick() {
    this.accumulator++;
    if (this.accumulator >= this.turns) {
      if (this.message) {
        add_text_sound_effect(this.entity, this.message);
      }
      return true;
    }
    return false;
  }
}
