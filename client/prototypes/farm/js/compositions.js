/** Compositions */
// These are like traits.

const Renderable = (self) => ({

  // Load the image.
  load_image: (asset) => {
    if (self._src != asset) {
      if (ASSETS[asset]) {
        self._src = asset;
        self._img = ASSETS[asset];
      }
      else {
        console.log(asset);
        ASSETS[asset] = ASSETS['missing_tile.png'];
      }
    }
  },

  // Render if image loaded.
  render: () => {
    // X and y properties are set by create_item().
    if (self._img && self._img.complete == true) {
      CONTEXT.drawImage(self._img, self.x * 32, self.y * 32);
    }
  }
});

const Triggerable = (self) => ({
  _events: [],
  
  listen: (name) => {
    self._events.push(name);
    EventManager.listen(name, self);
  },

  destroy_events: () => {
    for (let name of self._events) {
      EventManager.stop_listening(name, self);
    }
  },
});

class SystemInterface {
  tick() { };
  // Run after tick, should be used for reseting data or cleanup.
  post_tick() { };
  render() { };
  canvas_wheel() { };

  water(position) { };
  pick(position) { };
}

function sum2d(a, b) { return {x: a.x + b.x, y: a.y + b.y } }
function subtract2d(a, b) { return {x: a.x - b.x, y: a.y - b.y } }
function is_equal2d(a, b) { return a && b && a.x == b.x && a.y == b.y; }

var Reflection = new class {
  constructor() {
    this.classes = [];
  }

  register(_class) {
    this.classes.push(_class);
  }

  subclasses(_class) {
    return this.classes.filter((i) => {
      return i.prototype instanceof _class;
    });
  }
}();
