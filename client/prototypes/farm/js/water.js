
class GroundSystem extends SystemInterface {
  constructor(width, height) {
    super();
    this.grid = [];
    for (let y = 0; y < height; y++) {
      this.grid[y] = [];
      for (let x = 0; x < width; x++) {
        this.grid[y][x] = new GroundTile(x, y, this);
      }
    }
    this.can_render = false;
  }

  tick() {
    for (let y in this.grid) {
      for (let x in this.grid[y]) {
        if (this.grid[y][x] > 0) {
          this.grid[y][x]--;
        }
      }
    }
  }

  get_ground(position) {
    return this.grid[position.y] && this.grid[position.y][position.x] ? this.grid[position.y][position.x] : null;
  }

  get_water(position) {
    let ground = this.get_ground(position);
    return ground ? ground.water_level : 0;
  }

  water(position, quantity = 50) {
    // Can't water if there is a building or cart on it.
    if (Trains.get_cart(position)) {
      return;
    }

    let ground = this.get_ground(position);
    if (ground) {
      ground.water(quantity);
      add_water_effect(ground);
    }
  }

  render() {
    CONTEXT.font = 'italic 400 6px sans-serif';
    CONTEXT.fillStyle = "#55f";
    for (let y in this.grid) {
      for (let x in this.grid[y]) {
        let ground = this.grid[y][x];
        if (ground.water_level) {
          ground.render();
          if (this.can_render) {
            CONTEXT.fillText(ground.water_level, x * 32 + 10, y * 32 + 16);
          }
        }
      }
    }
  }

  tick() {
    for (let y in this.grid) {
      for (let x in this.grid[y]) {
        let ground = this.grid[y][x];
        ground.tick();
      }
    }
  }

   post_tick() {
    for (let y in this.grid) {
      for (let x in this.grid[y]) {
        let ground = this.grid[y][x];
        ground.post_tick();
      }
    }
  }

};

class GroundTile {
  static title = "Ground";
  
  constructor(x, y, system) {
    Object.assign(this, Renderable(this));
    this.x = x;
    this.y = y;
    this.system = system;
    this.water_level = 0;
    this.need_update = false;
    this.update_image();
  }

  tick() {
    if (this.water_level > 0) {
      this.water_level--;
      if (this.water_level == 0) {
        this.update();
      }
    }
  }

  water(quantity) {
    let old_water_level = this.water_level;
    this.update_image();
    this.water_level+= quantity;
    if (this.water_level < 0) {
      this.water_level = 0;
    }
    if ((old_water_level == 0 && this.water_level > 0)
    || (this.water_level == 0 && old_water_level > 0)) {
      this.update();
    }
  }

  // Set for update.
  update() {
    this.need_update = true;
    let neighbors = get_neighbor_positions(this);
    for (let i in neighbors) {
      let ground = this.system.get_ground(neighbors[i]);
      if (ground) {
        ground.need_update = true;
      }
    }
  }

  post_tick() {
    if (this.need_update) {
      this.need_update = false;
      this.update_image();
    }
  }

  update_image() {
    // Set the image.
    let name = ['wet_ground'];
    let neighbors = get_neighbor_positions(this);
    for (let i in neighbors) {
      let ground = this.system.get_ground(neighbors[i]);
      if (ground && ground.water_level > 0) {
        name.push(i);
      }
    }
    
    name = name.join('_') + '.png';
    this.load_image(name);
  }
    
}

