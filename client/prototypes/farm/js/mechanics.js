function class_to_string(class_) {
  if (typeof class_ == "string") {
    return class_;
  }
  return class_.name;
}

class MechanicsSystem extends SystemInterface {
  constructor(width, height) {
    super();
    this.width = width;
    this.height = height;
    // We need to pre create the containers so Vue can observe it.
    this.reset();
  }

  reset() {
    this.items = {};
    let containers = Reflection.subclasses(Mechanism);
    for (let container of containers) {
      this.items[class_to_string(container)] = [];
    }
  }

  get_containers() {
    return [
      'CartDisassembler',
      'CartMoverStopper',
      'CartReverser',
      'PressurePlate',
      'Sprinkler',
      'Timer',
      'WaterDetector',
    ];
  }

  tick() {
    for (let i in this.items) {
      this.items[i].forEach((item) => { item.tick() });
    }
  }

  post_tick() {
    for (let i in this.items) {
      this.items[i].forEach((item) => { item.post_tick() });
    }
  }

  create(position, type) {
    let container = class_to_string(type);
    if (Resources.can_buy(container)) {
      let item = this.get(position, container);
      if (!item) {
        if (typeof type == 'string') {
          type = eval(type);
        }
        item = new type(position.x, position.y, this);
        if (!this.items[container]) {
          this.items[container] = [];
        }
        Resources.buy(container);
        this.items[container].push(item);
      }
      return item;
    }
    return null;
  }

  get(position, type) {
    let container = class_to_string(type);
    for (let i in this.items[container]) {
      let item = this.items[container][i];
      if (is_equal2d(position, item)) {
        return item;
      }
    }
    return null;
  }
  
  delete(position, type) {
    let container = class_to_string(type);
    for (let i in this.items[container]) {
      let item = this.items[container][i];
      if (is_equal2d(item, position)) {
        if (typeof this.items[container][i].destroy === 'function') {
          this.items[container][i].destroy();
        }
        let price = Resources.get_price(container);
        if (price) {
          Resources.add(price);
          add_static_text_effect(position, "sold");
        }
        this.items[container][i] = null;
        this.items[container].splice(i, 1);
        break;
      }
    }
  }

};

class Mechanism {
  static title() { return "Mechanism"; }
  static ui() { return []; }

  constructor(x, y, system) {
    this.connections = [];
    this.x = x;
    this.y = y;
    this.system = system;
    // Check if the item was processed on this turn already.
    this.processed = false;
    // This will set the title to the instance from the static value.
    this.title = this.constructor.title();
  }

  // Connect to any item.
  connect_to(position, type) {
    let target = null;
    let container = class_to_string(type);
    if (container == 'tracks') {
      target = Trains.get_track(position);
    }
    else {
      target = this.system.get(position, type);
    }
    if (target && target.trigger instanceof Function) {
      this.connections.push(target);
    }
  }

  serialize() {
    let buffer = { x: this.x, y: this.y, image: this._src, _connections: [] };
    for (let i in this.connections) {
      let target = this.connections[i];
      buffer._connections.push({x: target.x, y: target.y, 'class': target.constructor.name});
    }
    return buffer;
  }

  destroy() {
    if (typeof this.destroy_events == 'function') {
      this.destroy_events();
    }
  }

  trigger() { }
  tick() { }
  post_tick() { }
}
Reflection.register(Mechanism);

/** When a locomotive moves over it, it will trigger something its connected to*/
class PressurePlate extends Mechanism {
  static title() { return "Pressure plate"; }

  static ui() {
    return [
      {
        'image': 'pressure_plate.png',
        'title': 'Pressure plate',
        'callback': (mouse) => { Mechanics.create(mouse, PressurePlate); },
      },
      {
        'title': 'Remove',
        'callback': (mouse) => { Mechanics.delete(mouse, PressurePlate); },
      },
    ];
  }

  constructor(x, y, system) {
    super(...arguments);
    Object.assign(this, Renderable(this), Triggerable(this));
    this.load_image('pressure_plate.png');
    this.processed = false;

    // Listen to events.
    this.listen('locomotive_move');
  }

  serialize() {
    let buffer = super.serialize();
    buffer.processed = this.processed;
    return buffer;
  }

  trigger() {
    if (!this.processed) {
      for (let i in this.connections) {
        if (this.connections[i]) {
          this.connections[i].trigger();
        }
      }
      add_text_sound_effect(this, 'click');
    }
    this.processed = true;
  }

  post_tick() {
    this.processed = false;
  }
}
Reflection.register(PressurePlate);

/** Disconnects the cart on top of it from the front cart */
class CartDisassembler extends Mechanism {
  static title() { return "Cart disassembler"; }

  static ui() {
    return [
      {
        'image': 'cart_disassembler.png',
        'title': 'Cart disassembler',
        'callback': (mouse) => { Mechanics.create(mouse, CartDisassembler); },
      },
      {
        'title': 'Remove',
        'callback': (mouse) => { Mechanics.delete(mouse, CartDisassembler); },
      },
    ];
  }
  
  constructor(x, y, system) {
    super(...arguments);
    Object.assign(this, Renderable(this));
    this.load_image('cart_disassembler.png');
  }

  trigger() {
    let cart = Trains.get_cart(this);
    if (cart && cart.front) {
      add_static_text_effect(this, 'disassembling');
      cart.front.back = null;
      cart.front = null;
    }
  }
}
Reflection.register(CartDisassembler);

/** Stops/move a cart when triggered. */
class CartMoverStopper extends Mechanism {
  static title() { return "Cart mover/stopper"; }
  
  static ui() {
    return [
      {
        'image': 'cart_mover_stopper_active.png',
        'title': 'Cart mover',
        'callback': (mouse) => { Mechanics.create(mouse, CartMoverStopper); },
      },
      {
        'image': 'cart_mover_stopper_inactive.png',
        'title': 'Cart stopper',
        'callback': (mouse) => {
          let i = Mechanics.create(mouse, CartMoverStopper);
          if (i) {
            i.active = false;
            i.update_image();
          }
        },
      },
      {
        'title': 'Remove',
        'callback': (mouse) => { Mechanics.delete(mouse, CartMoverStopper); },
      },
    ];
  }
      
  constructor(x, y, system) {
    super(...arguments);
    Object.assign(this, Renderable(this));
    this.active = true; // active = green.
    this.update_image();
  }

  update_image() {
    const image = this.active ? 'cart_mover_stopper_active.png' : 'cart_mover_stopper_inactive.png';
    this.load_image(image);
  }

  serialize() {
    let buffer = super.serialize();
    buffer.active = this.active;
    return buffer;
  }

  trigger() {
    this.active = !this.active;
    if (this.active) {
      let cart = Trains.get_cart(this);
      if (is_main_locomotive(cart)) {
        cart.move();
      }
    }
    this.update_image();
  }

  tick() {
    if (!this.active) {
      let cart = Trains.get_cart(this);
      if (is_main_locomotive(cart)) {
        cart.stop();
      }
    }
  }
}
Reflection.register(CartMoverStopper);

/** If there is a locomotive on top of it, make it move backward once its
 * triggered. */
class CartReverser extends Mechanism {
  static title() { return "Cart reverser"; }

  static ui() {
    return [
      {
        'image': 'cart_reverser.png',
        'title': 'Cart reverser',
        'callback': (mouse) => { Mechanics.create(mouse, CartReverser); },
      },
      {
        'title': 'Remove',
        'callback': (mouse) => { Mechanics.delete(mouse, CartReverser); },
      },
    ];
  }
  
  constructor(x, y, system) {
    super(...arguments);
    Object.assign(this, Renderable(this));
    this.load_image('cart_reverser.png');
    // Prevent the same locomotive get triggered again before leaving.
    this.triggered_locomotive = null;
  }

  trigger() {
    let cart = Trains.get_cart(this);
    if (cart) {
      cart = get_first_cart(cart);
      if (is_main_locomotive(cart)) {
        // Compare 2 objects doesn't work, but we can compare the position.
        if (this.triggered_locomotive != cart) {
          if (cart.forward) {
            cart.backward();
          }
          else {
            cart.move();
          }
          add_static_text_effect(this, 'reversing');
          this.triggered_locomotive = cart;
        }
      }
    }
    else {
      this.triggered_locomotive = null;
    }
  }

  post_tick() {
    if (this.triggered_locomotive) {
      let cart = Trains.get_cart(this);
      if (!cart || cart != this.triggered_locomotive) {
        this.triggered_locomotive = null;
      }
    }
  }

  serialize() {
    let buffer = super.serialize();
    if (this.triggered_locomotive) {
      buffer.triggered_locomotive = {
        x: this.triggered_locomotive.x,
        y: this.triggered_locomotive.y,
      };
    }
    return buffer;
  }
}
Reflection.register(CartReverser);

class Timer extends Mechanism {
  static title() { return "Timer"; }

  static ui() {
    return [
      {
        'image': 'timer_active0.png',
        'title': 'Timer',
        'callback': (mouse) => { Mechanics.create(mouse, Timer); },
      },
      {
        'title': 'Remove',
        'callback': (mouse) => { Mechanics.delete(mouse, Timer); },
      },
    ];
  }

  constructor(x, y, system) {
    super(...arguments);
    Object.assign(this, Renderable(this));
    // If active then its counting.
    this.active = false;
    this.turns_to_trigger = 0;
    this.max_turns_to_trigger = 20;
    this.processed = false;
    this.update_image();
  }

  trigger() {
    if (!this.active) {
      // Reset and start counting.
      this.turns_to_trigger = this.max_turns_to_trigger;
      // Do not run tick on the same turn this is triggered.
      if (!this.processed) {
        this.turns_to_trigger++;
      }
      this.active = true;
    }
  }

  update_image() {
    if (!this.active || this.turns_to_trigger <= 0) {
      this.load_image('timer_inactive.png');
    }
    else {
      // Get a number from 1 to 4 representing how much of the timer passed
      // already.
      let timer_phase = Math.ceil(100 / this.max_turns_to_trigger * (this.max_turns_to_trigger - this.turns_to_trigger) / 25);
      this.load_image(`timer_active${timer_phase}.png`);
    }
  }

  tick() {
    this.processed = true;
    if (this.active) {
      this.turns_to_trigger--;
      if (this.turns_to_trigger <= 0) {
        this.active = false;
        for (let i in this.connections) {
          if (this.connections[i]) {
            this.connections[i].trigger();
          }
        }
        add_text_sound_effect(this, 'plém');
      }
    }
    this.update_image();
  }

  post_tick() {
    this.processed = false;
  }

  serialize() {
    let buffer = super.serialize();
    buffer.active = this.active;
    buffer.turns_to_trigger = this.turns_to_trigger;
    buffer.max_turns_to_trigger = this.max_turns_to_trigger;
    buffer.processed = this.processed;
    return buffer;
  }

}
Reflection.register(Timer);

class Sprinkler extends Mechanism {
  static title() { return "Sprinkler"; }

  static ui() {
    return [
      {
        'image': 'sprinkler_active.png',
        'title': 'Sprinkler',
        'callback': (mouse) => { Mechanics.create(mouse, Sprinkler); },
      },
      {
        'title': 'Remove',
        'callback': (mouse) => { Mechanics.delete(mouse, Sprinkler); },
      },
    ];
  }
  
  constructor(x, y, system) {
    super(...arguments);
    Object.assign(this, Renderable(this));
    this.active = false;
    this.neighbors = [];
    this.processed = false;
    this.reach = 2;
    this.update_image();
  }

  update_neighbors() {
    // oy = offset y
    this.neighbors = [];
    for (let oy = this.reach * -1;  oy <= this.reach; oy++) {
      for (let ox = this.reach * -1; ox <= this.reach; ox++) {
        let x = this.x + ox;
        let y = this.y + oy;
        if ((x >= 0 && x < this.system.width) && (y >= 0 && y < this.system.height)) {
          this.neighbors.push({'x': x, 'y': y});
        }
      }
    }
  }

  update_image() {
    const src = this.active ? 'sprinkler_active.png' : 'sprinkler_inactive.png';
    this.load_image(src);
  }

  water() {
    this.update_neighbors();
    for (let i in this.neighbors) {
      Ground.water(this.neighbors[i], 10);
    }
  }

  trigger() {
    this.active = !this.active;
    if (this.active && this.processed) {
      this.water();
    }
  }
  
  tick() {
    this.processed = true;
    if (this.active) {
      this.water();
    }
  }

  post_tick() {
    this.processed = false;
  }

  serialize() {
    let buffer = super.serialize();
    buffer.active = this.active;
    buffer.reach = this.reach;
    return buffer;
  }

}
Reflection.register(Sprinkler);

class WaterDetector extends Mechanism {
  static title() { return "Water detector"; }
  
  static ui() {
    return [
      {
        'image': 'water_detector.png',
        'title': 'Water detector',
        'callback': (mouse) => { Mechanics.create(mouse, WaterDetector); },
      },
      {
        'title': 'Remove',
        'callback': (mouse) => { Mechanics.delete(mouse, WaterDetector); },
      },
    ];
  }
  
  constructor(x, y, system) {
    super(...arguments);
    Object.assign(this, Renderable(this));
    this.active = false;
    this.water_level = 0;
    this.load_image('water_detector.png');
  }

  trigger() {
  }

  tick() {
    let old_water_level = this.water_level;
    this.water_level = Ground.get_water(this);
    if (old_water_level == 0 && this.water_level > 0) {
      for (let i in this.connections) {
        if (this.connections[i]) {
          this.connections[i].trigger();
        }
      }
    }   
  }

}
Reflection.register(WaterDetector);
