Vue.component('track-button', {
  props: { tool: String },
  data: () => {
    return { active: false };
  },
  created() {
    document.addEventListener('ui:set_tool', () => {
      this.active = this.tool == window.tool;
    });
  },
  methods: {
    setTool() {
      window.tool = window.tool == this.tool ? null : this.tool;
      document.dispatchEvent(new CustomEvent('ui:set_tool'));
    }
  },
  computed: {
    src() {
      return `assets/${this.tool}.png`;
    },
  },
  template: `<button :class="{active: active}" @click="setTool()"><img :src="src" width="32" height="32"></button>`,
});

Vue.component('mechanism-button', {
  props: { mechanism: String },
  data: () => {
    return {
      active: false,
      _class: Function,
      tool: null,
    };
  },
  created() {
    document.addEventListener('ui:set_tool', () => {
      this.tool = window.tool;
    });
    this._class = eval(this.mechanism);
  },
  methods: {
    setTool(callback) {
      window.tool = window.tool == callback ? null : callback;
      this.tool = window.tool;
      document.dispatchEvent(new CustomEvent('ui:set_tool'));
    }
  },
  computed: {
    buttons() {
      return this._class.ui();
    }
  },
  template: `
<div>
  <button :class="button.callback == tool ? 'active' : ''" v-for="button in buttons" @click="setTool(button.callback)">
    <img v-if="button.image != undefined" class="button-icon" :src="'assets/' + button.image" width="32" height="32">
    {{ button.title }}
  </button>
</div>
`,
});

Vue.component('ui', {
  data: function() {
    return {
      tool: null,
      T_sections: [],
      mechanics: '',
      trains: '',
      highligths: [],
      highlight_target: null,
      triggers_connections_active: false,
      cart_list_active: false,
      interval: window.interval
    };
  },
  created: function() {
    this.mechanics = Mechanics;
    this.trains = Trains;
    this.setTool(null);
  },
  methods: {
    addMoney(money = 1000) {
      Resources.add({'money': money});
    },
    skipTurns(turns) {
      skip_turns(turns);
    },
    setTool(tool) {
      this.tool = this.tool == tool ? null : tool;
      window.tool = this.tool;
      this.highlight_target = null;
      this.highligths = [];
      this.update();
      document.dispatchEvent(new CustomEvent('ui:set_tool'));
    },
    save() {
      let serialized = JSON.stringify(serialize());
      localStorage.setItem('saved_game_state', serialized);
    },
    load() {
      let saved_game_state = localStorage.getItem('saved_game_state');
      if (saved_game_state) {
        unserialize(JSON.parse(saved_game_state));
      }
    },
    // Update non reactive items.
    update() {
      // Populate the T_sections when the tracks changes.
      this.T_sections = [];
      for (let y in this.trains.tracks) {
        for (let x in this.trains.tracks[y]) {
          if (this.trains.tracks[y][x] && this.trains.tracks[y][x].sections == 3) {
            let item = this.trains.tracks[y][x];
            this.T_sections.push({
              text: `${item.x} ${item.y}`,
              value: `tracks:${item.x},${item.y}`,
              position: `${item.x},${item.y}`,
            });
          }
        }
      }
    },
    connectTriggerables() {
      const connection = document.querySelector('[name=connection]').value;
      const connection_to = document.querySelector('[name=connection_to]').value;
      // Trigger
      let [type, position] = connection.split(':');
      let [x, y] = position.split(',');
      let trigger = this.mechanics.get({x, y}, type);

      if (!trigger) {
        return;
      }
      
      // Target.
      [type, position] = connection_to.split(':');
      [x,y] = position.split(',');
      trigger.connect_to({x, y}, type);
    },
    deleteConnection() {
      const c = document.querySelector('[name=connections]');
      for (let i = 0; i < c.selectedOptions.length; i++) {
        let value = c.selectedOptions[i].value;
        let [container, delta, connection] = value.split(':');
        this.mechanics.items[container][delta].connections.splice(connection, 1);
      }
    },
    cartMove(move) {
      const c = document.querySelector('[name=cart_list]');
      for (let i = 0; i < c.selectedOptions.length; i++) {
        let value = c.selectedOptions[i].value;
        if (is_locomotive(this.trains.carts[value])) {
          if (move) {
            this.trains.carts[value].move();
          }
          else {
            this.trains.carts[value].stop();
          }
        }
      }
    },
    cartMoveBackward() {
      const c = document.querySelector('[name=cart_list]');
      for (let i = 0; i < c.selectedOptions.length; i++) {
        let value = c.selectedOptions[i].value;
        if (is_locomotive(this.trains.carts[value])) {
          this.trains.carts[value].backward();
        }
      }
    },
    connectCarts() {
      const c1 = document.querySelector('[name=cart_list_connect1]').value;
      const c2 = document.querySelector('[name=cart_list_connect2]').value;
      if (c1 != c2) {
        this.trains.carts[c1].set_back(this.trains.carts[c2]);
      }
    },
    demo() {
      demo();
    },
    highlight(event) {
      if (event.target) {
        if (event.target.hasAttribute('data-position')) {
          this.highlight_target = event.target;
        }
      }
    },
    tick() {
      if (this.highlight_target) {
        let positions = this.highlight_target.getAttribute('data-position').split(';');
        this.highligths = [];
        for (let i in positions) {
          let [x,y] = positions[i].split(',');
          this.highligths.push({x, y});
        }
      }
    },
    // Render on canvas.
    render() {
      this.tick(); // @shitty
      if (this.highligths.length) {
        CONTEXT.beginPath();
        CONTEXT.lineWidth = "3";
        CONTEXT.strokeStyle = "yellow";

        for (let i in this.highligths) {
          CONTEXT.rect(this.highligths[i].x * 32, this.highligths[i].y * 32, 32, 32);
        }
        CONTEXT.stroke();
      }
    },
    getPosition(object) {
      let position = `${object.x},${object.y}`;
      if (object.target_x) {
        position += `;${object.target_x},${object.target_y}`;
      }
      return position;
    },
    setInterval(interval) {
      this.interval = interval;
      set_tick_interval(interval);
    },
    renderGroundSystem(event) {
      Ground.can_render = event.target.checked;
    },
  },
  watch: {
    T_sections: (value) => { },
  },
  computed: {
    triggers() {
      let items = [];
      for (let container in this.mechanics.items) {
        for (let i in this.mechanics.items[container]) {
          let trigger = this.mechanics.items[container][i];
          for (let c in trigger.connections) {
            let target = trigger.connections[c];
            items.push({
              text: `${trigger.title} ${trigger.x}/${trigger.y} >> ${target.title} ${target.x}/${target.y}`,
              value: `${container}:${i}:${c}`,
              position: `${trigger.x},${trigger.y};${target.x},${target.y}`,
            });
          }
        }
      }
      return items;
    },
    locomotives() {
      let locomotives = [];
      for (i in this.trains.carts) {
        if (is_main_locomotive(this.trains.carts[i])) {
          locomotives.push({
            x: this.trains.carts[i].x,
            y: this.trains.carts[i].y,
            name: this.trains.carts[i].title,
            id: i,
          });
        }
      }
      return locomotives;
    },
    triggerables() {
      let triggerables = {};
      for (let container in this.mechanics.items) {
        if (this.mechanics.items[container].length) {
          // Get the name from the first item.
          let name = this.mechanics.items[container][0].title;
          console.log(this.mechanics.items[container][0]);
          triggerables[name] = [];
          for (let i in this.mechanics.items[container]) {
            let item = this.mechanics.items[container][i];
            triggerables[name].push({
              text: `${item.x} ${item.y}`,
              value: `${container}:${item.x},${item.y}`,
              position: `${item.x},${item.y}`,
            });
          }
        }
      }
      console.log(triggerables);
      return triggerables;
    }
  },
  template: `
<div>
  <div>
    <button @click="demo()"><b>DEMO</b></button>
    <button @click="save()">Save</button>
    <button @click="load()">Load</button>
  </div>
  <div class="turn-controllers">
    <button :class="{active: interval == null}" @click="setInterval(null)">⏹️</button>
    <button :class="{active: interval == 500}" @click="setInterval(500)">▶️</button>
    <button :class="{active: interval == 200}" @click="setInterval(200)">⏩️️</button>
    <button @click="skipTurns(1)">+1</button>
    <button @click="skipTurns(10)">+10️️</button>
    <button @click="skipTurns(50)">+50️</button>
    <button @click="skipTurns(100)">+100️</button>
  </div>
  <hr>
  <div>
    <button :class="{ active: tool == 'watering_can'}" @click="setTool('watering_can')"><img src="assets/tool_watering_can.png" width="32" height="32"></button>
    <button :class="{ active: tool == 'scythe'}" @click="setTool('scythe')"><img src="assets/tool_scythe.png" width="32" height="32"></button>
    <button :class="{ active: tool == 'hoe'}" @click="setTool('hoe')"><img src="assets/tool_hoe.png" width="32" height="32"></button>
  </div>
  <hr>
  <div>
    <button :class="tool == 'track' ? 'active' : ''" @click="setTool('track')">Auto track</button>
    <button :class="tool == 'remove_track' ? 'active' : ''" @click="setTool('remove_track')">Remove track</button><br>
    <track-button tool="track_top_bottom"></track-button>
    <track-button tool="track_right_left"></track-button>
    <track-button tool="track"></track-button>
    <br>
    <track-button tool="track_top"></track-button>
    <track-button tool="track_right"></track-button>
    <track-button tool="track_bottom"></track-button>
    <track-button tool="track_left"></track-button>
    <br>
    <track-button tool="track_top_right"></track-button>
    <track-button tool="track_right_bottom"></track-button>
    <track-button tool="track_bottom_left"></track-button>
    <track-button tool="track_top_left"></track-button>
    <br>
    <track-button tool="track_top_right_bottom__right_bottom"></track-button>
    <track-button tool="track_top_right_bottom__top_right"></track-button>
    <track-button tool="track_top_bottom_left__top_left"></track-button>
    <track-button tool="track_top_bottom_left__bottom_left"></track-button>
    <br>
    <track-button tool="track_top_right_left__top_left"></track-button>
    <track-button tool="track_top_right_left__top_right"></track-button>
    <track-button tool="track_right_bottom_left__bottom_left"></track-button>
    <track-button tool="track_right_bottom_left__right_bottom"></track-button>
    <br>
    <hr>
  </div>

  <mechanism-button mechanism="PressurePlate"></mechanism-button>
  <mechanism-button mechanism="CartMoverStopper"></mechanism-button>
  <mechanism-button mechanism="CartReverser"></mechanism-button>
  <mechanism-button mechanism="CartDisassembler"></mechanism-button>
  <mechanism-button mechanism="Timer"></mechanism-button>
  <mechanism-button mechanism="Sprinkler"></mechanism-button>
  <mechanism-button mechanism="WaterDetector"></mechanism-button>

  <button :class="{active: triggers_connections_active}" @click="triggers_connections_active = !triggers_connections_active">Connections / Triggers</button>
  <div v-if="triggers_connections_active" class='fieldset'>
    <select name="connection" @click="highlight($event)" size="15">
      <optgroup v-for="(group, name) in triggerables" :label="name">
        <option v-for="option in group" :value="option.value" :data-position="option.position">{{ option.text }}</option>
      </optgroup>
    </select>
    <label>To</label>
    <select name="connection_to" @click="highlight($event)" size="15">
      <optgroup v-for="(group, name) in triggerables" :label="name">
        <option v-for="option in group" :value="option.value" :data-position="option.position">{{ option.text }}</option>
      </optgroup>
      <optgroup v-if="T_sections.length" label="Track (T section)">
        <option v-for="option in T_sections" :value="option.value" :data-position="option.position">{{ option.text }}</option>
      </optgroup>
    </select>
    <br>
    <button @click="connectTriggerables()">Connect</button>
    <hr>
    <div>
      <select name="connections" @click="highlight($event)" size="10">
        <option v-for="option in triggers" :value="option.value" :data-position="option.position">{{ option.text }}</option>
      </select>
      <br>
      <button @click="deleteConnection()">Delete connection</button>
    </div>
  </div>
  <hr>
  <div>
    <button :class="tool == 'locomotive' ? 'active' : ''" @click="setTool('locomotive')">Locomotive</button>
    <button :class="tool == 'wagon' ? 'active' : ''" @click="setTool('wagon')">Wagon</button>
    <button :class="tool == 'water_wagon' ? 'active' : ''" @click="setTool('water_wagon')">Water Wagon</button>
    <button :class="tool == 'remove_cart' ? 'active' : ''" @click="setTool('remove_cart')">Remove Cart</button>
  </div>
  <div>
    <button :class="{active: cart_list_active}" @click="cart_list_active = !cart_list_active">Cart list</button>
    <div v-if="cart_list_active" class='fieldset'>
      <select name="cart_list" multiple @click="highlight($event)">
        <option v-for="o in locomotives" :value="o.id" :data-position="getPosition(o)">
          {{ o.title }} {{ o.x }}/{{ o.y }}
        </option>
      </select>
      <div>
        <button @click="cartMove(true)">Move</button>
        <button @click="cartMove(false)">Stop</button>
        <button @click="cartMoveBackward()">Move backward</button>
        <div><small>Only locomotives can move and stop.</small></div>
      </div>
      <div>
        <hr>
        <small>
          Connect locomotives and wagons. <br>
          Locomotives and Wagons auto connect if they<br>
          are build behind another locomotive/wagon. <br>
          Locomotives can also connect to other locomotives<br>
          but no wagons can be in front of locomotives.
        </small>
        <div>
          <label>front
            <select name="cart_list_connect1" @click="highlight($event)">
              <option v-for="(o, key) in trains.carts" :value="key" :data-position="getPosition(o)">
                {{ o.title }} {{ o.x }}/{{ o.y }}
              </option>
            </select>
          </label><br>
          <label>back
            <select name="cart_list_connect2" @click="highlight($event)">
              <option v-for="(o, key) in trains.carts" :value="key" :data-position="getPosition(o)">
                {{ o.title }} {{ o.x }}/{{ o.y }}
              </option>
            </select>
          </label>
          <button @click="connectCarts">Connect</button>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div>
    <button :class="tool == 'crop_avocado' ? 'active' : ''" @click="setTool('crop_avocado')">Avocado</button>
    <button :class="tool == 'crop_cassava' ? 'active' : ''" @click="setTool('crop_cassava')">Cassava</button>
    <button :class="tool == 'crop_coffee' ? 'active' : ''" @click="setTool('crop_coffee')">Coffee</button>
    <button :class="tool == 'crop_corn' ? 'active' : ''" @click="setTool('crop_corn')">Corn</button><br>
    <button :class="tool == 'crop_cucumber' ? 'active' : ''" @click="setTool('crop_cucumber')">Cucumber</button>
    <button :class="tool == 'crop_eggplant' ? 'active' : ''" @click="setTool('crop_eggplant')">Eggplant</button>
    <button :class="tool == 'crop_grapes' ? 'active' : ''" @click="setTool('crop_grapes')">Grapes</button>
    <button :class="tool == 'crop_lemon' ? 'active' : ''" @click="setTool('crop_lemon')">Lemon</button><br>
    <button :class="tool == 'crop_melon' ? 'active' : ''" @click="setTool('crop_melon')">Melon</button>
    <button :class="tool == 'crop_orange' ? 'active' : ''" @click="setTool('crop_orange')">Orange</button>
    <button :class="tool == 'crop_pineapple' ? 'active' : ''" @click="setTool('crop_pineapple')">Pineapple</button>
    <button :class="tool == 'crop_potato' ? 'active' : ''" @click="setTool('crop_potato')">Potato</button><br>
    <button :class="tool == 'crop_rice' ? 'active' : ''" @click="setTool('crop_rice')">Rice</button>
    <button :class="tool == 'crop_rose' ? 'active' : ''" @click="setTool('crop_rose')">Rose</button>
    <button :class="tool == 'crop_strawberry' ? 'active' : ''" @click="setTool('crop_strawberry')">Strawberry</button>
    <button :class="tool == 'crop_sunflower' ? 'active' : ''" @click="setTool('crop_sunflower')">Sunflower</button><br>
    <button :class="tool == 'crop_tomato' ? 'active' : ''" @click="setTool('crop_tomato')">Tomato</button>
    <button :class="tool == 'crop_tulip' ? 'active' : ''" @click="setTool('crop_tulip')">Tulip</button>
    <button :class="tool == 'crop_turnip' ? 'active' : ''" @click="setTool('crop_turnip')">Turnip</button>
    <button :class="tool == 'crop_wheat' ? 'active' : ''" @click="setTool('crop_wheat')">Wheat</button><br>
    <button :class="tool == 'remove_crop' ? 'active' : ''" @click="setTool('remove_crop')">Remove Crops</button>
  </div>
  <hr>
  <button @click="addMoney(100)">+100 money</button>
  <button @click="addMoney(1000)">+1000 money</button>
  <hr>
  <label><input type="checkbox" @click="renderGroundSystem($event)">render water levels.</label>
</div>
`,
});


Vue.component('ui-top', {
  data: function() {
    return {
      resources: Resources.resources,
      global: window.Global,
    };
  },
  template: `
<div>
  <div class='ui-top-right'>
    <span>Turn: {{ global.turns }}</span>
  </div>
  <span v-for="(quantity, resource) in resources" class="resource"><img :src="'assets/resource_' + resource + '.png'"> {{ quantity }}</span>
</div>
`,
});
