/**
 * Serialize and unserialize the game state.
 */

function dump_state() {
  let buffer = JSON.stringify(serialize());
  console.log(buffer);
}

function serialize() {
  let buffer = {};

  // Mechanics and trains.
  buffer.mechanics = {
    items: {},
  };
  buffer.trains = {};

  for (let type in Mechanics.items) {
    buffer.mechanics.items[type] = [];
    for (let i in Mechanics.items[type]) {
      let item = Mechanics.items[type][i];
      buffer.mechanics.items[type].push(item.serialize());
    }
  }
  buffer.trains.tracks = [];
  for (let y in Trains.tracks) {
    for (let x in Trains.tracks[y]) {
      if (Trains.tracks[y][x]) {
        let track = Trains.tracks[y][x];
        buffer.trains.tracks.push(track.serialize());
      }
    }
  }

  buffer.trains.carts = [];
  for (let c in Trains.carts) {
    let cart = Trains.carts[c];
    buffer.trains.carts.push({
      x: cart.x,
      y: cart.y,
      direction: cart.direction,
      _type: cart.constructor.name,
      _front: cart.front ? {x: cart.front.x, y: cart.front.y} : null,
      _back: cart.back ? {x: cart.back.x, y: cart.back.y} : null,
    });
  }

  // Crops.
  buffer.crops = [];
  for (let y in Crops.grid) {
    for (let x in Crops.grid[y]) {
      let crop = Crops.grid[y][x];
      if (crop) {
        buffer.crops.push({
          _type: crop.constructor.name,
          x: crop.x,
          y: crop.y,
          turn: crop.turn,
          phase: crop.phase,
          turns_to_die: crop.turns_to_die,
        });
      }
    }
  }

  return buffer;
}

function unserialize(data) {
  // Mechanics.
  Mechanics.reset();
  Trains.reset();

    // Create tracks.
  for (let t in data.trains.tracks) {
    let track_data = data.trains.tracks[t];
    let track = Trains.create_track(track_data);
    Object.assign(track, track_data);
  }
  
  for (let container in data.mechanics.items) {
    for (let i in data.mechanics.items[container]) {
      let item_data = data.mechanics.items[container][i];
      let item = null;
      switch (container) {
        case 'CartReverser':
          item = Mechanics.create(item_data, container);
          Object.assign(item, item_data);
          item.load_image(item._src);
          break;

        case 'CartMoverStopper':
          item = Mechanics.create(item_data, container);
          Object.assign(item, item_data);
          item.update_image();
          break;

        case 'PressurePlate':
          item = Mechanics.create(item_data, container);
          Object.assign(item, item_data);
          item.load_image(item._src);
          break;

        case 'CartDisassembler':
          item = Mechanics.create(item_data, container);
          Object.assign(item, item_data);
          item.load_image(item._src);
          break;

        case 'Timer':
          item = Mechanics.create(item_data, container);
          Object.assign(item, item_data);
          item.update_image();
          break;

        case 'Sprinkler':
          item = Mechanics.create(item_data, container);
          Object.assign(item, item_data);
          item.update_neighbors();
          item.update_image();
          break;
      }
    }
  }

  for (let container in Mechanics.items) {
    for (let i in Mechanics.items[container]) {
      switch (container) {
        case 'CartReverser':
        case 'CartMoverStopper':
        case 'PressurePlate':
        case 'CartDisassembler':
        case 'Timer':
        case 'Sprinkler':
          let item = Mechanics.items[container][i];
          for (let c in item._connections) {
            let connection = item._connections[c];
            if (connection['class'] == 'Track') {
              let target = Trains.get_track(connection);
              item.connections.push(target);
            }
            else {
              //let target_container = Mechanics.items[class_to_string[connection['class']]];
              let target = Mechanics.get(connection, connection['class']);
              item.connections.push(target);
            }
          }
          break;          
      }
    }
  }

  // Create carts.
  for (let c in data.trains.carts) {
    let cart_data = data.trains.carts[c];
    let type = eval(cart_data._type);
    let cart = Trains.create_cart(cart_data, type);
    if (cart) {
      Object.assign(cart, cart_data);
      cart.update_image();
    }
  }
  // Connect carts.
  for (let c in Trains.carts) {
    let cart = Trains.carts[c];
    cart.front = null;
    cart.back = null;
    if (cart._front) {
      cart.set_front(cart._front);
    }
    if (cart._back) {
      cart.set_back(cart._back);
    }
  }

  // Create crops.
  Crops.reset();
  for (let c in data.crops) {
    let crop_data = data.crops[c];
    let type = eval(crop_data._type);
    let crop = Crops.plant(crop_data, type);
    if (crop) {
      Object.assign(crop, crop_data);
      crop.update_image();
    }
  }
}
