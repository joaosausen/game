// Global variables.

//init_entities_test(app);

// Game -> Stage -> Map/Entities

// Test.

Game.pushState(STATE_MENU_State);

let ticker = PIXI.Ticker.shared;

// Delta time in miliseconds;
ticker.add(async function (dt) {
  Game.tick(ticker.elapsedMS / 1000);
});
