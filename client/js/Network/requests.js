/**
 * Requests are packets sent to the server.
 */

function request_move(direction_x, direction_y) {
  // Get values from entity.
  const packet_id = 1;
  const myself = Game.myself || null;
  if (!myself) {
    return;
  }
  Socket.send([packet_id, _pack_float(direction_x), _pack_float(direction_y)]);
}

function send_map_loaded(id) {
}

function request_entity(entity_id) {
}

function request_login(user, password) {
  Socket.connect(() => {
    let packet_id = 4;
    Socket.send([packet_id, user, password]);
  });
}

function request_stage_list() {
  Game.data_stages = [];
  let packet_id = 5;
  Socket.send([packet_id]);
}

function request_select_stage(id) {
  // Same as stage list.
  let packet_id = 6;
  Socket.send([packet_id, id]);
}

function request_kick_ball(pos_x, pos_y) {
  let packet_id = 7;
  Socket.send([packet_id, pos_x, pos_y]);
}
