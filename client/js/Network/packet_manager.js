/**
 * Handle packets from/to server.
 *
 * Singleton.
 */
var PacketManager = new class {

  constructor() {
    // Set a property to check if integers are big endian.
    const uInt32 = new Uint32Array([0x00000001]);
    const uInt8 = new Uint8Array(uInt32.buffer);
    Object.defineProperties(this, {
      'IS_BIG_ENDIAN': { value: (uInt8[0] != 1), writable: false },
    });
  }

  /**
   * Convert integer to 1's complement, little endian Uint8Array.
   */
  numberToUint8Array(number) {
    let int32 = new Int32Array(1);
    int32[0] = number;
    if (this.IS_BIG_ENDIAN) {
      return new Uint8Array(int32.buffer);
    }
    return new Uint8Array(int32.buffer).reverse();
  }


  /**
   * Receives an array of mixed data and transform into a array of bytes.
   *
   * This properly formats data to be sent to the server, serializing integers
   * and strings as bytes. Numbers are directly transformed into 32bits, strings
   * are encoded using TextEncoder, the string size is assigned to the beggining
   * of the string.
   *
   * @param data
   *   An array of integers/strings to be compressed in a Uint8Array.
   */
  encode(data) {
    const encoder = new TextEncoder();
    let buffer_length = 0;
    for (let i in data) {
      if (typeof data[i] === 'string') {
        // Encode the string and replace the string with a array with a integer
        // containing the length and the encoded string, we call flat() to
        // normalize it.
        // This transforms ["something"] into [Uint8Array, Uint8Array] where the
        // first array is the length and the second the text.
        let encoded = encoder.encode(data[i]);
        let length = this.numberToUint8Array(encoded.length);
        data[i] = [length, encoded];
        buffer_length += 4 + encoded.length;
      }
      else {
        // Assume it is a integer.
        data[i] = this.numberToUint8Array(data[i]);
        buffer_length += 4;
      }
    }
    data = data.flat();

    // Join all bytes in a single Uint8Array.
    let buffer = new Uint8Array(buffer_length);
    let offset = 0;
    for (let i in data) {
      buffer.set(data[i], offset);
      offset += data[i].length;
    }
    return buffer;
  }

  /**
   * Extract numbers from a DataView.
   *
   * @param data
   *   A DataView object.
   * @param offset
   *   The offset to start reading from.
   * @param size
   *   The return value size in bytes.
   */
  extract(data, offset = 0, size = 4) {
    switch (size) {
      case 4:
        return data.getInt32(offset);
      case 2:
        return data.getInt16(offset);
      case 1:
      default:
        return data.getInt8(offset);
    }
  }

  /**
   * Process data received from the server.
   *
   * Based on packet_db, this will build a array of numbers respecting the size
   * of entry in the packet and call the callback on the packet_db. data can
   * contain multiple packets sent by the server.
   *
   * @param data
   *   A DataView object.
   * @param offset
   *   The offset to start reading from.
   */
  decode(data, offset = 0) {
    if (offset >= data.byteLength) {
      // Done decoding.
      return;
    }

    // Get the first int to use as the packet id.
    const packet_id = data.getInt32(offset);
    if (!Game.packet_db[packet_id]) {
      // Assume everything else is corrupted.
      console.log(`[PacketManager::decode()] Packet ${packet_id} doesn't exist.`);
      return;
    }

    // Clone packet, so we can manipulate it.
    let packet = [...Game.packet_db[packet_id]];
    let callback = packet.shift();
    let callback_arguments = [];

    // Extract arguments from buffer, iterator here is considering the
    // packet_id.
    const decoder = new TextDecoder();
    let iterator = offset + 4;
    for (let argument_size of packet) {
      let argument = null;
      if (argument_size < 0) {
        // If it is an array, read a integer to get the argument size and point
        // iterator to beggining of array.
        const array_size = data.getInt32(iterator);
        iterator += 4;

        // Extract the array, use a  special case for "chars", transform into
        // string.
        if (argument_size == -1) {
          const array = new Uint8Array(data.buffer, iterator, array_size);
          argument = decoder.decode(array);
        }
        else {
          // Else, extract values from array.
          let array_argument = [];
          for (let i = 0; i < array_size; i++) {
            array_argument.push(this.extract(data, iterator + (i * argument_size), argument_size));
          }
          argument = array_argument;
        }
        iterator += array_size * Math.abs(argument_size);
      }
      else {
        // If thats not an array, just extract a number.
        argument = this.extract(data, iterator, argument_size);
        iterator += argument_size;
      }
      callback_arguments.push(argument);
    }

    // Call the callback with the calculated arguments.
    callback(...callback_arguments);
    this.decode(data, iterator);
  }

}();
