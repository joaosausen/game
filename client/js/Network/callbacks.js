/**
 * Callbacks from the packet_db.
 *
 * These are functions called directly by the server, they are mapped from
 * packets.
 */

/**
 * This same function exists on the server.
 */
function _unpack_float(number) {
  return number / 100;
}

/**
 * This same function exists on the server.
 */
function _pack_float(number) {
  return number * 100;
}

function create_entity(entity_id, entity_type, alignment, position_x, position_y) {
  let Stage = Game.getState().stage || null;
  if (Stage) {
    Stage
      .createEntity(entity_id, entity_type, alignment)
      .setPosition(position_x, position_y);
  }
}

function delete_entity(entity_id) {
  let Stage = Game.getState().stage || null;
  if (Stage) {
    Stage.deleteEntity(entity_id);
  }
}

// Instantly set an entity position.
function set_entity_position(entity_id, x, y) {
  let Stage = Game.getState().stage || null;
  if (Stage) {
    let entity = Stage.entity(entity_id);
    if (entity) {
      entity.setPosition(x, y);
    }
  }
}

function entity_move(entity_id, x, y, direction_x, direction_y, velocity) {
  let Stage = Game.getState().stage || null;
  if (Stage) {
    let entity = Stage.entity(entity_id);
    if (entity) {
      entity.move(_unpack_float(x), _unpack_float(y), _unpack_float(direction_x), _unpack_float(direction_y), _unpack_float(velocity));
    }
  }
}

function load_map(map_id) {
  let Stage = Game.getState().stage || null;
  if (Stage) {
    Stage.destroy();
  }
  let state = Game.pushState(STATE_IN_GAME_State);
  Stage = state.stage;
  Stage.loadMap(map_id);
}

function disconnect() {
  // This is for documentation only, this function is never called.
}

function popup_message(type, message) {
  const titles = { '0': 'Error' };
  const title = titles[type] || '';
  let event = new CustomEvent('popup-message', {
    'detail': {
      'title': title,
      'message': message,
    },
  });
  document.dispatchEvent(event);
}

// @todo send map information.
// @todo request the stage list instead of letting the server sending it
// automatically.
function stage_list(stage_id, players_online) {
  if (Game.state != Game.STATE_SELECT_STAGE) {
    Game.pushState(STATE_SELECT_STAGE_State);
  }

  Game.data_stages.push({
    'id': stage_id,
    'players_online': players_online,
  });
}

function login_successful() {
  Game.pushState(STATE_SELECT_STAGE_State);
}
