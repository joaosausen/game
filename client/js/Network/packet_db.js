/**
 * Data with all packets and their callbacks.
 *
 * In javascript every number is a Number, there are no shorts or integers at
 * all. The char and int identifiers are there just to identify how many bytes
 * we need to extract from the buffer to form the number we will use as a
 * argument in the callback.
 *
 * The first 4 bytes in a packet (int) are the packet identifier, we use to
 * identify the callback we should call.
 * chars and ints are arrays of values that have variable sizes. These
 * arrays are streamed as a integer representing the array size plus the array.
 *
 * As noted, we don't send floats, instead we send data as integers and the
 * respective callback will divide it by 100. This way we have a float with 2
 * digits of precision.
 *
 * @see PacketManager::decode()
 */

(function(Game) {
  // Types.
  const char   = 1;   //  8 bits
  const int    = 4;   // 32 bits
  const chars  = -1;  // (32 + 8 * array size) bits
  const ints   = -4;  // (32 + 32 * array size) bits

  Game.packet_db = {
    // packet identifier: [callback, data, data, data...]
    1: [create_entity, int, int, int, int, int],
    2: [entity_move, int, int, int, int, int, int],
    3: [delete_entity, int],
    4: [popup_message, int, chars],
    5: [stage_list, int, int],
    6: [load_map, int],
    7: [login_successful],
    100: [disconnect]

    // requests
  };
})(Game);
