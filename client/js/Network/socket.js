/**
 * Setup a connection.
 */
var Socket = new class {

  /**
   * Construct a Socket.
   */
  constructor(address) {
    this.address = address;
  }

  /**
   * Return if client is connected.
   */
  connected() {
    return this.connection && this.connection.readyState == WebSocket.OPEN;
  }

  /**
   * Connect to the server.
   *
   * @param callback
   *   A function to execute once the websocket connect.
   */
  connect(callback = null) {
    // If already connected, just call the callback and return.
    if (this.connected()) {
      if (callback) {
        callback();
      }
      return;
    }

    this.connection = new WebSocket(this.address);
    this.connection.onopen = (event) => {
      console.log('[socket] Connection open.');
      if (callback) {
        callback();
      }
    };

    this.connection.onclose = function(event) {
      if (event.wasClean) {
        console.log('[socket] Connection closed.');
      } else {
        console.log('[socket] Connection died.');
      }
      Game.disconnect();
    };

    this.connection.onerror = function(error) {
      console.log(`[socket] Error: ${error.message}`);
    };

    this.connection.onmessage = function(event) {
      new Response(event.data).arrayBuffer().then(
        array_buffer => {
          let buffer = new DataView(array_buffer);
          PacketManager.decode(buffer);
        }
      );
    };

  }

  /**
   * Get an array and send everything as a Uint8Array of integers.
   *
   * @param data
   *   Array of numbers and strings.
   */
  send(data) {
    if (!Array.isArray(data)) {
      data = [data];
    }

    let packet = PacketManager.encode(data);
    this.connection.send(packet);
  }
//}('ws://127.0.0.1:5999/127.0.0.1:5000');
}('ws://192.168.0.102:5999/127.0.0.1:5000');
//}('ws://sausen.hopto.org:5999/127.0.0.1:5000');
