/**
 * Ball class, singleton.
 */
class Ball extends Entity {

  /**
   * Construct a ball.
   */
  constructor() {
    super();

    /**
     * Properties.
     */

    // Entity direction in x and y axis, possible values -1, 0 and 1.
    this.direction_x = 0;
    this.direction_y = 0;

    // Entity velocity in pixels per second.
    this.velocity = 100;

    // Entity type name.
    this.name = 'Ball';

    /** @PixiJS */
    this.sprite = PIXI.Sprite.from('assets/ball.png');
    this.sprite.anchor.set(0.5);
    // Reference from the sprite to this entity.
    this.sprite.entity = this;
  }

}
