
/**
 * Handle events on STATE_MENU.
 *
 * Singleton.
 */
class STATE_MENU_State extends StateBase {
  constructor() {
    super();
    this.id = Game.STATE_MENU;
  }  
}
