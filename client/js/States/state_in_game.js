
/**
 * Handle events on STATE_IN_GAME.
 */
class STATE_IN_GAME_State extends StateBase {
  constructor() {
    super();

    this.id = Game.STATE_IN_GAME;

    /**
     * Stage the entity is connected to, the server handle multiple stages, in
     * the client we will always have one stage only.
     */
    this.stage = new Stage();

    // Used to calculate the direction the player pretend to move.
    this.direction = { x: 0, y: 0 };
  }

  /**
   * Proxy for request_move.
   *
   * Send the data to the server and immediately set the entity directions.
   */
  requestMoveEntity(value) {
    if (!isNaN(value.x)) {
      this.direction.x = value.x;
    }
    if (!isNaN(value.y)) {
      this.direction.y = value.y;
    }

    // If we are moving on diagonal, we reduce the velocity to the square root
    // of 2 divided by 2. This only works because we move on angles divisible
    // by 45. In any case, the server always fixes these values. This is just
    // to prevent to much desync.
    if (this.direction.x != 0 && this.direction.y != 0) {
      this.direction.x = this.direction.x < 0 ? -0.7 : 0.7;
      this.direction.y = this.direction.y < 0 ? -0.7 : 0.7;
    }
    request_move(this.direction.x, this.direction.y);
  }

  /**
   * @see EventManager.
   */
  leftClick(event, hint, object) {
    let myself = Game.myself || null;
    if (!myself) {
      return;
    }

    // Translate click.
    const click = event.data.global;
    let point = Game.app.stage.toLocal({'x': click.x, 'y': click.y});
    request_kick_ball(point.x, point.y);
  }

  /**
   * @see EventManager.
   */
  rightClick(event, hint, object) { }

  /**
   * @see EventManager.
   */
  keyDown(event) {
    if (!event.repeat) {
      let myself = Game.myself || null;
      if (!myself) {
        return;
      }

      switch (event.key) {
        case 'w': return this.requestMoveEntity({y: -1});
        case 'a': return this.requestMoveEntity({x: -1});
        case 's': return this.requestMoveEntity({y: 1});
        case 'd': return this.requestMoveEntity({x: 1});
        case 'F1':
          Game.ui_character = !Game.ui_character;
          break;

      }
    }
  }

  /**
   * @see EventManager.
   */
  keyUp(event) {
    let myself = Game.myself || null;
    if (!myself) {
      return;
    }

    let key = EventManager.key;
    let v = 0;

    switch (event.key) {
      case 'w':
        v = key.s == true ? 1 : 0;
        return this.requestMoveEntity({y: v});

      case 'a':
        v = key.d == true ? 1 : 0;
        return this.requestMoveEntity({x: v});

      case 's':
        v = key.w == true ? -1 : 0;
        return this.requestMoveEntity({y: v});

      case 'd':
        v = key.a == true ? -1 : 0;
        return this.requestMoveEntity({x: v});

    }
  }

  /**
   * @see EventManager.
   */
  focusOut(event) {
    let myself = Game.myself || null;
    if (!myself) {
      return;
    }
    // Stop entity when losing focus.
    myself.direction_x = 0;
    myself.direction_y = 0;
    request_move(this.direction.x, this.direction.y);
  }

  /**
   * @see EventManager.
   */
  destroy() {
    this.stage.destroy();
    Game.myself = null;
  }

  /**
   * @see StateBase::tick().
   */
  tick(delta) {
    // Update entity.
    for (let i in this.stage.entities) {
      this.stage.entities[i].update(delta);
    }

    // Move camera to "myself".
    if (Game.myself != null) {
      Game.app.stage.pivot.copyFrom(Game.myself.sprite.position);
    }

  }

}
