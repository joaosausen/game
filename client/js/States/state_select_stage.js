
/**
 * Handle events on STATE_SELECT_STAGE.
 *
 * Singleton.
 */
class STATE_SELECT_STAGE_State extends StateBase {
  constructor() {
    super();
    this.id = Game.STATE_SELECT_STAGE;
  }

  /**
   * @see StateBase::active().
   */
  active() {
    // Request the stage list.
    request_stage_list();
  }

}
