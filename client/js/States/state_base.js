
/**
 * Base classe for states.
 */
class StateBase {

  /**
   * Destroy the state.
   *
   * Performs cleanup as needed.
   */
  destroy() { }

  /**
   * Callback for when the state is set as active.
   *
   * This is called on constructing and when another state is destroyed and this
   * one is set as the active.
   */
  active() { }

  /**
   * Runs on every PIXI tick.
   */
  tick(delta) { }
  
  /**
   * @see EventManager.
   */
  leftClick(event, hint, object) { }

  /**
   * @see EventManager.
   */
  rightClick(event, hint, object) { }

  /**
   * @see EventManager.
   */
  keyDown(event) { }

  /**
   * @see EventManager.
   */
  keyUp(event) { }

  /**
   * @see EventManager.
   */
  focusOut(event) { }

}
