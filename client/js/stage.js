
/**
 * Class for a stage.
 *
 * A stage is a instance of a map, it contains a map and all the entities in
 * there. When a server requires the user to load a map it basically says we
 * should start a new instance and create this stage class.
 * We only have one stage active at a time, we would still want to go back to a
 * map instance somehow, so we can keep it open until the server communicates
 * we can remove it.
 */
class Stage {

  /**
   * Creates the stage class.
   *
   * It is created empty and must be populated as needed.
   */
  constructor(id, map, app) {
    // @todo, these parameters are not passed yet.
    // this.id = id;
    this.map = null;
    // this.app = app;
    this.entities = {};
  }

  /**
   * Create a new entity and store in the stage.
   *
   * If the id already exists, it will be ignored.
   *
   * @param id
   *   The entity id, matching the server id.
   * @param id_type
   *   The entity type, player, ball, etc.
   * @param alignment
   *   The entity alignment, a number representing it.
   *   0 = myself, 1 = ally, 2 = enemy
   */
  createEntity(id, id_type, alignment) {
    if (this.entities[id]) {
      return this.entities[id];
    }

    const class_name = entity_db[id_type][2] || null;
    switch (class_name) {
      case 'Ball':
        this.entities[id] = new Ball();
        break;

      default:
      case 'Player':
        this.entities[id] = new Player(id_type, alignment);
        if (alignment == Player.MYSELF) {
          Game.myself = this.entities[id];

          // Teste.
          Game.app.stage.position.set(Game.app.screen.width/2, Game.app.screen.height/2);
        }
        break;
    }

    /** @PixiJS */
    Game.app.stage.addChild(this.entities[id].sprite);
    return this.entities[id];
  }

  /**
   * Delete an entity and destroy it.
   * @param id
   *   The entity id, matching the server id.
   */
  deleteEntity(id) {
    if (this.entities[id]) {
      Game.app.stage.removeChild(this.entities[id].sprite);
      delete(this.entities[id]);
    }
  }

  /**
   * Return a entity.
   *
   * @param id
   *   The entity id.
   */
  entity(id) {
    if (this.entities[id] == undefined) {
      request_entity(id);
    }
    return this.entities[id] || null;
  }

  /**
   * Destroy everything on this stage.
   */
  destroy() {
    // Remove all sprites from PIXI app.
    for (let i in this.entities) {
      Game.app.stage.removeChild(this.entities[i].sprite);
      delete(this.entities[i]);
    }
  }

  /**
   * Load a map to this stage.
   */
  loadMap(map_id) {
    this.map = new Map(map_id);
    this.map.create();
  }

}
