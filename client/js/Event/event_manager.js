
/**
 * Handle any event from the browser.
 *
 * Singleton.
 */
var EventManager = new class {

  /**
   * Constructs a EventManager class.
   *
   * Setup global events.
   */
  constructor() {
    let _this = this;

    /** @PixiJS */
    Game.app.renderer.plugins.interaction.on('mousedown', function(event) {
      _this.leftClick(event, 'map', this);
      event.stopPropagation();
    });
    Game.app.renderer.plugins.interaction.on('rightdown', function(event) {
      _this.rightClick(event, 'map', this);
      event.stopPropagation();
    });
    // Disable right click menu.
    document.addEventListener('contextmenu', (event) => {
      event.preventDefault();
    });

    Game.app.view.onmouseout = function(event) {
      _this.focusOut(event);
    };

    document.onkeydown = function(event) {
      // Ignore on some elements.
      const ignored_tags = ['INPUT'];
      if (ignored_tags.indexOf(event.target.tagName) >= 0) {
        return;
      }
      event = event || window.event;
      event.preventDefault();
      _this.keyDown(event);
    };

    document.onkeyup = function(event) {
      // Ignore on some elements.
      const ignored_tags = ['INPUT'];
      if (ignored_tags.indexOf(event.target.tagName) >= 0) {
        return;
      }
      event = event || window.event;
      event.preventDefault();
      _this.keyUp(event);
    };

    // Register the keys pressed/released.
    this.key = {};
  }

  /**
   * Handles a mouse left click.
   *
   * @param event
   *   The event object.
   * @param hint
   *   A hint to define where we clicked.
   * @param object
   *   The object being clicked, a sprite, window, etc.
   */
  leftClick(event, hint, object) {
    Game.getState().leftClick(event, hint, object);
  }

  /**
   * Handles a mouse right click.
   *
   * @param event
   *   The event object.
   * @param hint
   *   A hint to define where we clicked.
   * @param object
   *   The object being clicked, a sprite, window, etc.
   */
  rightClick(event, hint, object) {
    Game.getState().rightClick(event, hint, object);
  }

  /**
   * Handles a keyboard key click.
   *
   * @param event
   *   The event object.
   */
  keyDown(event) {
    // Register the key as pressed.
    this.key[event.key] = true;

    // Special case.
    if (event.key == 'Escape' && Game.state != Game.STATE_MENU) {
      Game.ui_esc_menu = !Game.ui_esc_menu;
      return;
    }
    Game.getState().keyDown(event);
  }

  /**
   * Handles a keyboard key release.
   *
   * @param event
   *   The event object.
   */
  keyUp(event) {
    // Register the key as released.
    this.key[event.key] = false;

    Game.getState().keyUp(event);
  }

  /**
   * Handle the mouse moving outside of the game.
   *
   * @param event
   *   The event object.
   */
  focusOut(event) {
    Game.getState().focusOut(event);
  }

}();
