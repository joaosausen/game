/**
 * Game entry point.
 */
// Global variable.
var Game = new class {

  /**
   * Construct a Game.
   */
  constructor() {
    Object.defineProperties(this, {
      'STATE_MENU': { value: 0, writable: false },
      'STATE_SELECT_STAGE': { value: 1, writable: false },
      'STATE_IN_GAME': { value: 2, writable: false },
    });

    // Reference to client entity.
    this.myself = null;

    // Game state id for Vue reference.
    this.state = this.STATE_MENU;

    // The states pile, the last state is the active one.
    this.states = [];

    // Other states.
    this.ui_esc_menu = false;
    this.ui_character = false;

    // Start PixiJS.
    this.app = new PIXI.Application({
      backgroundColor: 0x1099bb,
      view: document.getElementById('canvas'),
      autoResize: true,
      resolution: devicePixelRatio
    });
    // Resize canvas.
    this.overlay = document.getElementById('ui-overlay');

    window.addEventListener('resize', () => {
      this.overlay.style.width = window.innerWidth + 'px';
      this.overlay.style.height = window.innerHeight + 'px';
      this.app.renderer.resize(window.innerWidth, window.innerHeight);
    });
    this.overlay.style.width = window.innerWidth + 'px';
    this.overlay.style.height = window.innerHeight + 'px';
    this.app.renderer.resize(window.innerWidth, window.innerHeight);

    // Store global data.
    this.data_stages = []; // This is for stage listing.
  }

  /**
   * Get the active state, if there are none, create a instance of
   * STATE_MENU_State.
   */
  getState() {
    if (!this.states.length) {
      this.pushState(STATE_MENU_State);
    }
    return this.states[this.states.length - 1];
  }

  /**
   * Runs on state change.
   */
  onStateChange() {
    // Run the active method on active state.
    let state = this.getState();
    state.active();
    // Synchronize the fast reference to state.
    this.state = state.id;
  }

  /**
   * Add state and make it active.
   */
  pushState(state_class) {
    this.states.push(new state_class());
    this.onStateChange();
    return this.getState();
  }

  /**
   * Destroy the active state and return the new active one.
   */
  destroyState() {
    this.getState().destroy();
    this.states.pop();
    this.onStateChange();
    return this.getState();
  }

  /**
   * Handle a disconnect.
   */
  disconnect() {
    this.destroy();
  }

  /**
   * Destroy the game state and go back to menu.
   */
  destroy() {
    // This will delete all states and create a new menu one.
    for (let i in this.states) {
      this.destroyState();
    }
  }

  /**
   * Runs on evry tick from PIXI ticker.
   */
  tick(delta) {
    // Delegate to active state.
    this.getState().tick(delta);
  }

}();

/**
 * Helper mixin for UI elements.
 */
Vue.mixin({
  data: function() {
    return {
      STATE_MENU: false,
      STATE_SELECT_STAGE: false,
      STATE_IN_GAME: false,
      Game_reference: '',
    };
  },
  created: function() {
    this.Game_reference = Game;
  },
  watch: {
    'Game_reference.state': {
      handler(value) {
        this.STATE_MENU = Game.STATE_MENU == value;
        this.STATE_SELECT_STAGE = Game.STATE_SELECT_STAGE == value;
        this.STATE_IN_GAME = Game.STATE_IN_GAME == value;
      }
    },
  },
});
