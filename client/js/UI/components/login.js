Vue.component('menu-login', {
  data: function() {
    return {
      user: 'a',
      password: 'b',
      game: '',
    }
  },
  methods: {
    login() {
      UI.loginComponentOk(this.user, this.password);
      this.password = '';
    },
  },
  template: `
<div v-if="STATE_MENU" class='game-menu ui'>
  <div class='ui-title'>Login</div>
  <div><input type="text" v-model="user" placeholder="user"></div>
  <div><input type="password" v-model="password" placeholder="password"></div>
  <div class='ui-action'>
    <button @click="login">Login</button>
  </div>
</div>
`,
});

new Vue({ el: '#login' });

Vue.component('menu-select-stage', {
  methods: {
    selectStage(id) {
      UI.selectStage(id);
    }
  },
  template: `
<div v-if="STATE_SELECT_STAGE" class='game-menu ui'>
  <div class='ui-title'>Select a stage</div>
  <div class="stage__container">
    <div v-for="stage in Game_reference.data_stages" class='stage' v-bind:style="{ backgroundImage: 'url(http://placeimg.com/280/200/any)' }" @click="selectStage(stage.id)">
      <div class='stage__name'>Taquara</div>
      <div class='stage__players-online'>{{ stage.players_online }} &#x26BD;</div>
    </div>
  </div>
</div>
  `,
});
new Vue({ el: '#select-stage' });
