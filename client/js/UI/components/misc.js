Vue.component('popup-message', {
  data: function() {
    return {
      title: '',
      message: '',
      display: false,
    }
  },
  created() {
    document.addEventListener('popup-message', (event, data) => {
      this.title = event.detail.title;
      this.message = event.detail.message;
      this.display = true;
    });
  },
  template: `
<div v-if="display" class='game-menu ui popup-message'>
  <div class='ui-title'>{{ title }}</div>
  <p>{{ message }}</p>
  <div class='ui-action'>
    <button @click="display = false">Ok</button>
  </div>
</div>
`,
});

Vue.component('esc-menu', {
  methods: {
    selectStage() {
      Game.destroyState();
      this.close();
    },
    logout() {
      Socket.connection.close();
      this.close();
    },
    close() {
      this.Game_reference.ui_esc_menu = false;
    },
  },
  template: `
<div v-if="Game_reference.ui_esc_menu" class='game-menu ui'>
  <div class='ui-title'>Menu</div>
  <div class='ui-button-menu'>
    <button @click="selectStage">Back to stage list</button>
    <button @click="logout">Logout</button>
  </div>
</div>
`,
});

new Vue({ el: '#misc' });
