Vue.component('equip-slot', {
  props: {
    description: null,
    image: null,
    label: null,
  },
  template:`
<div class="equip-slot">
  {{ image }}
  <div class="equip-label">{{ label }}</div>
  <div class="equip-description">{{ description }}</div>
</div>
`,
});

Vue.component('equipment', {
  data: function() {
    return {
      
    }
  },
  methods: {
    
  },
  created() {

  },
  template: `
<div v-if="Game_reference.ui_character" class='game-menu ui'>
  <div class='ui-title'>Character</div>
  <div class='equipment'>
    <div class="character-equipment-left">
      <equip-slot label="Head" description="Hat"></equip-slot>
      <equip-slot label="Lower Head" description=""></equip-slot>
      <equip-slot label="Right Hand" description="Lance"></equip-slot>
      <equip-slot label="Back" description="Robe"></equip-slot>
      <equip-slot label="Accessory" description="Ring"></equip-slot>
    </div>
    <div>
      PLACEHOLDER
    </div>
    <div class="character-equipment-right">
      <equip-slot label="Neck" description="Necklace"></equip-slot>
      <equip-slot label="Body" description="Armor"></equip-slot>
      <equip-slot label="Left Hand" description="Lance"></equip-slot>
      <equip-slot label="Feet" description="Shoes"></equip-slot>
      <equip-slot label="Accessory" description="Ring"></equip-slot>
    </div>
  </div>
  <div class='ui-action'>
    <button @click="Game_reference.ui_character = !Game_reference.ui_character">Close</button>
  </div>
</div>
`,
});

new Vue({ el: '#ingame' });

// Caracteristicas
// Mutant
// Undead
// True Kin

// Specie
// Tipo pokemon?

// Class
// Alchemist
// Blacksmith
// Knight
// Archer
// Priest
// Wizard
// Sage
// Paladin
// Monk
// Bard
/*
<div class="character-specie">
      <span><label>Specie</label>Gengar</span>
      <span><label>Class</label>Alchemist</span>
      <span><label>Type</label>Mutant</span>
      <span>Specie</span>
      <p>Items strenght requirements reduced by 20%.</p>
      <p>+1 Strenght</p>
      <p>+1 Luck</p>
    </div>*/
