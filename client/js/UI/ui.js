/**
 * Handle the general UI.
 */
var UI = new class {

  /** Click on "ok" at login component */
  loginComponentOk(user, password) {
    request_login(user, password);
  }

  /** Select stage. */
  selectStage(id) {
    request_select_stage(id);
  }
  
}

/* This will unselect selected text. */
document.onclick = function(e) {
  if (window.getSelection) {
    window.getSelection().removeAllRanges();
  }
  else if (document.selection) {
    document.selection.empty();
  }
}
