/**
 * Entity class.
 */
class Player extends Entity {

  /**
   * Construct a new Entity.
   *
   * This class has the Stage as a factory, some properties are set there.
   *
   * @param id_type
   *   The entity type, to load from the entity_db.
   */
  constructor(id_type, alignment) {
    super();
    if (!entity_db[id_type]) {
      console.error(`[Entity::constructor] id_type ${id_type} doesn't exists`);
      id_type = 0;
    }

    /**
     * Properties.
     */

    // Entity direction in x and y axis, possible values -1, 0 and 1.
    this.direction_x = 0;
    this.direction_y = 0;

    // Entity velocity in pixels per second.
    this.velocity = 100;

    // Entity type name.
    this.name = entity_db[id_type][0];

    // Entity alignment.
    this.alignment = alignment;

    /** @PixiJS */
    this.sprite = PIXI.Sprite.from('assets/' + entity_db[id_type][1]);
    this.sprite.anchor.set(0.5);
    this.sprite.interactive = true;
    this.sprite.buttonMode = true;
    // Reference from the sprite to this entity.
    this.sprite.entity = this;
    // Bind click event to entity.
    this.sprite.on('rightdown', function(event) {
      EventManager.rightClick(event, 'entity', this);
      event.stopPropagation();
    });

    // Fast reference if the user have the ball.
    this.ball = true;
  }

}

// Defines.
Object.defineProperties(Player, {
  'MYSELF': { value: 0, writable: true },
  'ALLY':   { value: 1, writable: false },
  'ENEMY':  { value: 2, writable: false }
});
