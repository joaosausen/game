// entity id: [name, sprite, class]
var entity_db = {};

entity_db = {
  0: ['NoName', 'bunny.png', 'Player'], // Generic (this is a fallback for missing entities).
  1: ['Bunny', 'bunny.png', 'Player'],
  2: ['Ball', 'ball.png', 'Ball'],
};

/**
 * Entity class.
 */
class Entity {

  /**
   * Construct a new Entity.
   */
  constructor() {

  }

  /**
   * Synchronize position with the server.
   *
   * Entity update moves the entity in the client, this function interpolates
   * movement to try to reduce discrepancy between client and server.
   */
  move(x, y, direction_x, direction_y, velocity) {
    this.direction_x = direction_x;
    this.direction_y = direction_y;
    this.velocity = velocity;
    // Get the difference between server and client, divide it by interpolation
    // and move entity in that direction.
    const interpolation = 10;
    let diff_x = (x - this.sprite.x);
    let diff_y = (y - this.sprite.y);
    if (this.velocity > 0) {
      if (diff_x != 0) {
        this.direction_x += diff_x / interpolation / this.velocity;
      }
      if (diff_y != 0) {
        this.direction_y += diff_y / interpolation / this.velocity;
      }
    }
    return this;
  }

  /**
   * Immediatelly set the entity position.
   */
  setPosition(x, y) {
    this.sprite.x = x;
    this.sprite.y = y;
    return this;
  }

  /**
   * Update entity.
   */
  update(delta) {
    this.sprite.x += this.direction_x * this.velocity * delta;  
    this.sprite.y += this.direction_y * this.velocity * delta;
  }

}
