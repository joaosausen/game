
/**
 * Class for a stage.
 *
 * Singleton.
 *
 * A stage is a instance of a map, it contains a map and all the entities in
 * there. When a server requires the user to load a map it basically says we
 * should start a new instance and create this stage class.
 */
class Map {

  /**
   * Builds the map class.
   *
   * Maps are static, we can load it by id and it will be the same map we have
   * on the server.
   */
  constructor(id) {
    this.id = id;
    this.sprites = [];
  }


  /**
   * Load the map from a file.
   */
  loadMap() {
    // This is just a collision map for now.
    // @see load_map() on server.
    // @todo.
    this.name = "Mapa";
    this.tile_size = 32;
    this.tiles = [
      [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
      [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
      [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
      [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
      [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
      [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
      [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
      [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
      [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
      [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
      [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0]
    ];
  }

  /**
   * Properly create the map.
   */
  create() {
    this.loadMap();
    let i = 0;

    // Draw a single rectangle inside both goals.
    this.sprites[i] = new PIXI.Graphics();
    this.sprites[i].beginFill(0x57ad6e);
    this.sprites[i].drawRect(48, 288, 1056, 240);
    Game.app.stage.addChild(this.sprites[i]);
    i++;

    // Draw the lines.
    this.sprites[i] = new PIXI.Graphics();
    this.sprites[i].beginFill(0x57ad6e);
    this.sprites[i].lineStyle(10, 0xe2FFF9);
    this.sprites[i].drawRect(96, 48, 960, 768);
    Game.app.stage.addChild(this.sprites[i]);
    i++;

    // Create tilemap.
    for (let y in this.tiles) {
      for (let x in this.tiles[y]) {
        let tile = this.tiles[y][x];
        if (tile == 1) {
          this.sprites[i] = PIXI.Sprite.from('assets/wall.png');
          this.sprites[i].x = x * 48;
          this.sprites[i].y = y * 48;
          this.sprites[i].anchor.set(0, 0.5);
          Game.app.stage.addChild(this.sprites[i]);
          i++;
        }
      }
    }
  }

  destroy() {
    
  }

}
